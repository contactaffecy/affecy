//Register - profile
$(document).ready(function(){
  $('#fornick').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9._\b]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
  });
});

function checkInputs(inputs) {

    var filled = true;
    
    inputs.forEach(function(input) {
        
      if(input.value === "") {
          filled = false;
      }
    
    });
    
    return filled;
    
  }
  
var inputs = document.querySelectorAll("input");
var button = document.querySelector("button");
  
  inputs.forEach(function(input) {
      
    input.addEventListener("keyup", function() {
  
      if(checkInputs(inputs)) {
        //button.disabled = false;
        button.disabled = false
      } else {
        //button.disabled = true;
        button.disabled= true;
      }
  
    });
  
  });

  function showPass() {
    var tipo = document.getElementById("formpass1");
    var tipoc = document.getElementById("formpass2");
    if(tipo.type == "password" && tipoc.type == "password"){
        tipo.type = "text";
        tipoc.type = "text";
    }else{
        tipo.type = "password";
        tipoc.type = "password";
    }
  }

  // Self-executing function
  (function validateForm() {
      'use strict';
      window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');
          
          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                  if (form.checkValidity() === false) {
                      event.preventDefault();
                      event.stopPropagation();
                  }
                  form.classList.add('was-validated');
              }, false);
          });
      }, false);
  })();
