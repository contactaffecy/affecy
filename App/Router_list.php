<?php

namespace App;

use App\Controllers\HomeController;
use App\Controllers\RegisterController;
use App\Controllers\UserController;
use App\Router;
use App\Views\MainView;

require ('./../vendor/autoload.php');
$autoload = function($class){
   include($class.'.php');
};
spl_autoload_register($autoload);

$home = new HomeController();
$reg = new RegisterController();
$user = new UserController();



Router::get('/', function() use($home) {
    $home->homeSub();
});

Router::get('/home', function() use($home) {
    $home->home();
});

Router::get('/sub', function() use($home) {
    $home->homeSub();
});

Router::get('/learnmore', function() use($home) {
   $home->learnMore();
});

Router::get('/confirmation', function() use($home) {
  $home->subConfirmation();
});

/*** HOME */

/**Router::get('/register', function() use($reg) {
   $reg->register();
}); 

Router::get('/register', function() use($home) {
   $home->homeSub();
});

Router::get('/register/2', function() use($reg) {
   $reg->registerStep1();
});

Router::get('/login', function() use($home) {
   $home->login();
});

Router::get('/testereg', function() use($reg) {
   $reg->registerTest();
});

Router::get('/teste', function() use($home) {
   $home->teste();
});

/*** USER */

/**Router::get('/p/home', function() use($user) {
   $user->index();
});
Router::get('/p/settings', function() use($user) {
   $user->settings();
});
Router::get('/p/edit-profile', function() use($user) {
   $user->editProfile();
});
Router::get('/p/edit-info', function() use($user) {
   $user->index();
});

Router::get('/passRecovery', function() use($home) {
   $home->passRecovery();
});

Router::get('/passRecoveryCheck', function() use($home) {
   $home->passCheck();
});

Router::get('p/newPass',function () use($home){ //incluir redirecionamento
   $home->passNew();
});

Router::get('p/resetPassword',function () use($home){ //incluir redirecionamento
   $home->passCheck();
});

Router::get('p/loggout',function () use($user){
   $user->loggout();
});

Router::get('loggout',function () use($user){
   $user->loggout();
});*/

/** DEMAIS ROTAS */

Router::get('/?', function() use($home) {
   $home->error();
});

Router::get('/?/?', function() use($home) {
   $home->error();
});

Router::get('/?/?/?', function() use($home) {
   $home->error();
});

/*Router::get('/login', function() use($home) {
   $home->login();
});*/







