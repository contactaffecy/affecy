<?php

    namespace App;

    class Utility {

        public static function redirect($url){
            echo '<script>location.href="'.htmlspecialchars($url).'"</script>';
            die();
        }

        public static function alert($msg){
            echo '<script>alert("'.$msg.'")</script>';
            die();
        }
    }