<?php

    namespace App;

    class Conn {

        private static $pdo;

        public static function connect(){

            if(self::$pdo == null){
                try{
                    self::$pdo = new \PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME,DB_USER,DB_PASS, 
                        array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
                    self::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                }catch(\Exception $e){
                    echo 'Error connecting';
                    error_log($e->getMessage());
                    //die();
                }
            }

            return self::$pdo;
        }
    }