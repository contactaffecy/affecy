<?php

    namespace App\Views;

	class Mainview
	{
		public static $par = [];

		public static function setParam($par){
			self::$par = $par;
		}
		
		public static function render($fileName,$arr = [],$header = NULL,$footer = NULL, $titulo = NULL){
			if($header != NULL){
				include('pages/includes/'.$header);
			}

			include('pages/'.$fileName);
			
			if($footer != NULL){
				include('pages/includes/'.$footer);
			}
			
		
			die();
		}	
	}