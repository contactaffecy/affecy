<?php 
    switch (@$_GET['url']){
        case 'passRecoveryCheck':
            $show = 0;
            break;
        case 'home':
            $show = 0;
            break;
        default:
            $show = 1;
    }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Affecy 1.0.0">
    <title><?php echo $titulo ?></title>

    <link href="<?php echo APP_IMG ?>icon.ico" rel="shortcut icon" type="image/x-icon"/><link href="<?php echo APP_IMG ?>icon.ico" rel="apple-touch-icon"/>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css" rel="stylesheet" />
    <link href="<?php echo APP_CSS ?>default/debeta_1.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<?php 
    if ($show == 1){
    
?>
<nav class="menu-mob">
    <div class="row">
        <div class="col-lg-6">
            <a href="#" class="btn-mob"><span><i class="fa-solid fa-angle-left"></i></span></a>
        </div>

    </div>
</nav>

<?php
    }
?>
