<?php

    //recebe as informações do usuario logado
    $info = App\Models\UserModel::getUserId('user_email', $_SESSION['user_email']);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Affecy 1.0.0">
    <title><?php echo $titulo ?></title>

    <link href="<?php echo APP_IMG ?>icon.ico" rel="shortcut icon" type="image/x-icon"/><link href="<?php echo APP_IMG ?>icon.ico" rel="apple-touch-icon"/>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css" rel="stylesheet" />
    <link href="<?php echo APP_CSS ?>default/debeta_1.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://kit.fontawesome.com/87ab512a71.js" crossorigin="anonymous"></script>
</head>
<header>
    <nav id='menubar'>
   
    <ul>
        <li class="profile">
            <a href='<?= PATH ?>/p/home' alt="Menu" title="Menu">
                <div class="round" style="background-image: url(<?= APP_IMG.'/logo/logounique.png' ?>)"></div>
            </a>
        </li>
        <li>
            <a href='../loggout'><i class="fa-solid fa-arrow-right-from-bracket"></i></a>
        </li>
        <li>
            <a href='../p/settings'><i class="fa-solid fa-gear"></i></a>
        <li>
            <a href='http://' style="color:#ffab00">200 <i class="fa-solid fa-coins i-coin"></i></a>
        </li>
        <li>
            <a href='http://'><i class="fa-solid fa-square-poll-vertical fa-lg"></i>
                <span class="badge bg-danger badge-dot" title="New answer"></span>
            </a>
        </li>
        <li>
            <a href='http://'><i class="fa-solid fa-bell"></i></a>
        </li>
               
    </ul>
    </nav>
</header>
<body>