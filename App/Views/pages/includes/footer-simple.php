
  <footer class="footer-simple" id="pgsimple">
    <!-- Copyright -->
    <div class="row justify-content-center">
      <hr width="50%" style="color:#eeeeee">
      
      <div class="col-sm-8">
        <div class="text-center p-3" style="font-size:12px;color:#4f4f4f">
            <span class="footer-copy">Affecy.com © <?php echo date('Y') ?></span>
        </div>
      </div><!-- col-4-->

</div><!-- row -->

  </footer>

</body>
<!-- MDB -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ?>/Public/js/scripts.js"></script>


</html>