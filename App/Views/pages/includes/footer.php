
  <footer class="footer-simple">
    <!-- Copyright -->
    <div class="row">
      <hr width="50%" style="color:#eeeeee">
      
      <div class="col-sm-4">
        <div class="text-left p-3" style="font-size:12px;color:#4f4f4f">
            <span class="footer-copy">Affecy.com © <?php echo date('Y') ?></span>
        </div>
      </div><!-- col-4-->

      <div class="col-sm-4">
        <div class="text-center p-3" style="font-size:12px;color:#4f4f4f">
            <span class="footer-copy"><a href="#" >Privacy Policy</a></span>
            <span class="footer-copy">|</span>
            <span class="footer-copy"><a href="#">Terms of Use</a></span>
        </div>
      </div><!-- col-6-->

      <div class="col-sm-4" style="align-self:center;text-align:right">
        <div class="text-left p-3">
            <a href="https://facebook.com/affecy" title="Facebook Affecy" alt="Facebook Affecy" target="_blank" class="footer-link"><i class="fab fa-facebook" style="padding-right:14px"></i></a>
            <a href="https://instagram.com/affecyofficial" title="Instagram Affecy" alt="Instagram Affecy" target="_blank" class="footer-link"><i class="fab fa-instagram" style="padding-right:14px"></i></a>
            <a href="https://twitter.com/affecyofficial" title="Twitter Affecy" alt="Twitter Affecy" target="_blank" class="footer-link"><i class="fab fa-twitter" style="padding-right:14px"></i></a>
            <a href="https://tiktok.com/@affecy" title="TikTok Affecy" alt="TikTok Affecy" target="_blank" class="footer-link"><i class="fab fa-tiktok" style="padding-right:14px"></i></a>
        </div>
        </div><!-- col-4-->
    </div><!-- row -->

  </footer>

</body>
<!-- MDB -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.js"></script>
<script type="text/javascript" src="<?php echo PATH ?>/Public/js/scripts.js"></script>
<script>
  $(document).ready(function(){
    let txty = document.querySelector('input[name="ubday"]');
    txty.style.opacity = '1';
  });
</script>
<script>
  $('.navTrigger').click(function () {
      $(this).toggleClass('active');
      console.log("Clicked menu");
      $("#mainListDiv").toggleClass("show_list");
      $("#mainListDiv").fadeIn();
  });
</script>
</html>