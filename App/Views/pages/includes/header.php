
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Affecy 1.0.0">
    <title><?php echo $titulo ?></title>

    <link href="<?php echo APP_IMG ?>icon.ico" rel="shortcut icon" type="image/x-icon"/><link href="<?php echo APP_IMG ?>icon.ico" rel="apple-touch-icon"/>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css" rel="stylesheet" />
    <link href="<?php echo APP_CSS ?>default/debeta_1.css" rel="stylesheet">
    <link href="<?php echo APP_CSS ?>default/navbar.css" rel="stylesheet">
    <link href="<?php echo APP_CSS ?>default/loading.css" rel="stylesheet">
</head>
<header>
    <nav class="nav">
        <div class="container">
            <div class="logo">
                <a href="<?= PATH ?>"><img src="<?php echo APP_IMG ?>logo/logoaffecy-txt.png" style="width:80px"></a>
            </div>
            <div id="mainListDiv" class="main_list">
                <ul class="navlinks">

                <?php 
                    if(!isset($_SESSION['logado'])){
                ?>
                    <li><a href="learnmore" class="link-login">Learn More</a></li>
                   
                    <li><a href="#subs" class="home-navbar ">Register free</a></li>
                <?php 
                    }else{

                        echo '<li><a href="p/home/" class="home-navbar">My account</a></li>';

                    }
                ?>

                </ul>
            </div>
            <span class="navTrigger">
                <i></i>
                <i></i>
                <i></i>
            </span>
        </div>
    </nav>
</header>