
<body>
<div class="container" style="margin-top:45px">
    
    <section class="headline">
        <div class="row justify-content-center">
            
               <div class="col-md-4">
                   
                    <h1 class="headline-h">Feelings and time are important.</h1>
                    <p class="sub-desc" style="margin:20px 0">Test the combination before moving on to the date or dating. See if it really is for you.</p>
                   
                    <div class="reg">
                        <a href="register" class="btn-login bgradient text-center" style="margin:20px 0">Get started free</a>
                        <p align="center" class="complement">Have an account? <a href="<?php echo PATH ?>/login" title="Login Affecy" alt="Login Affecy" class="import"> Log in</a></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo APP_IMG ?>site/couple-3.png" style="width:100%">
                </div>
                <div class="space"></div>              
               
        </div><!-- row -->
    </section>
       

</div><!-- container -->

<section class="headline-s howork" id="features">
    <div class="container">
        <h2 class="headline-h h-bl">The next level of the match.</h2>
       
            <!-- ================== Features ================= -->
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="card" style="background-color:#ffffff;">
                        <div class="card-body">
                            <img src="<?php echo APP_IMG ?>/site/icon1.png">
                            <h2 class="card-title"> <span class="card-span">Decrease the fakes.</span>
                                Don't be fooled by the pretty face. Check if it really matches.
                            </h2>
                        </div> <!-- card-body -->
                    </div><!-- card -->
                </div><!-- col-md-4 -->

                <div class="col-md-3">
                    <div class="card" style="background-color:#ffffff;">
                        <div class="card-body">
                            <img src="<?php echo APP_IMG ?>/site/icon3.png" >
                            <h2 class="card-title"><span class="card-span">Technology.</span>
                                Answers that help you decide on your future dating.
                            </h2>
                        </div> <!-- card-body -->
                    </div><!-- card -->
                </div>

                <div class="col-md-3">
                    <div class="card" style="background-color:#ffffff;">
                        <div class="card-body">
                            <img src="<?php echo APP_IMG ?>/site/icon6.png">
                            <h2 class="card-title"><span class="card-span">Time.</span>
                                Save your time and feeling after the match.
                            </h2>
                        </div> <!-- card-body -->
                    </div><!-- card -->
                </div>

                <div class="col-md-3">
                    <div class="card" style="background-color:#ffffff;">
                        <div class="card-body">
                            <img src="<?php echo APP_IMG ?>/site/icon7.png">
                            <h2 class="card-title"><span class="card-span">Self-reliance</span>
                                Find out if there really is a match and don't lose your confidence.
                            </h2>
                        </div> <!-- card-body -->
                    </div><!-- card -->
                </div>
            </div><!-- row -->

        <div class="space"></div>
    </div><!--container-->
</section>

<section class="headline" style="padding:50px 20px">
    <div class="container">
        <h2 class="headline-h h-bl">Make your dating decision easier with technology.</h2>
            
        <div class="row justify-content-center" style="display:-webkit-flex;
    -webkit-align-items: center;display:flex;align-items: center;">
            <div class="col-md-4">
                <p class="sub-desc">.</p>
                <div class="how">
                    <span class="how-number">1</span>
                    <span class="how-title w5">Secret answers</span>
                    <span class="how-desc">Create the answers and choose the level of happiness in each one.</span>
                </div>

                <div class="how">
                    <span class="how-number">2</span>
                    <span class="how-title w5">Share with crush</span>
                    <span class="how-desc">Send your Affecy smart form to crush.</span>
                </div>

                <div class="how">
                    <span class="how-number">3</span>
                    <span class="how-title w5">Magic happens</span>
                    <span class="how-desc">Our algorithm will work with some variables and show the degree of combination between you.</span>
                </div>

                <div class="how">
                    <span class="how-number">4</span>
                    <span class="how-title w5">Match rate</span>
                    <span class="how-desc">View the match rate and responses that most closely match you.</span>
                </div>
                <div style="text-align:center">
                    <a href="register" alt="Create an account" title="Create an account" class="btn-login text-center">View my match rate</a>
                </div>
            </div><!-- col -->
                
            <div class="col-md-4">
                <img src="<?php echo APP_IMG ?>site/affecy_card1.png" alt="Card Affecy" title="Card Affecy" class="img-site-card">
            </div><!-- col -->
            
        </div><!-- row -->
    </div>
</section>

    <hr style="text-align:center;color:#f5f5f5;">

    <section class="textend">
        <div class="row justify-content-center">
            <div class="col-sm-8 .offset-sm-1">
                <h4 class="textend">
                Relationships are complex. People think they know what they want in their partner,
                but the truth is, the choice is not a conscious one.<br>
                Affecy make this choice more conscious. 
                </h4>
            </div>
        </div>
    </section>
<!--<section id="cards">
    <div class="card-esp" style='background-image: url(Public/img/prof/prof-card.png)'>
        <span>You have a new combo answer!</span>
        <p>Check out the result and who knows this could be your next love.</p>    
    </div>
</section>-->
<!-- Function used to shrink nav bar removing paddings and adding black background -->
<script>
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('.nav').addClass('affix');
            console.log("OK");
        } else {
            $('.nav').removeClass('affix');
        }
    });
</script>





    
