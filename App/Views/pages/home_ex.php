<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Hugo 0.98.0">
    <title>Affecy - The next level of the match</title>

    <link href="https://uploads-ssl.webflow.com/6296d203d2a1456a68f7a5c9/629c4a0a33cf8026734d05c3_logo_4_affecy(simbol).png" rel="shortcut icon" type="image/x-icon"/><link href="https://uploads-ssl.webflow.com/6296d203d2a1456a68f7a5c9/629c4a254ba1c5d02ed9b6e7_logo_4_affecy_256.png" rel="apple-touch-icon"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link href="<?php echo APP_CSS ?>default/alpha.css" rel="stylesheet">
</head>

<body>
    <div class="header" style="background-image: url('<?php echo APP_IMG ?>site/home-alpha-1.png')">
        <nav>
            <img src="<?php echo APP_IMG ?>logo/logowhite-txt.png" width="20%">
            <ul>
                <li><a href="#">About</a></li>
                <li><a href="#">Sign up</a></li>
            </ul>
        </nav>

        <div class="text-box">
            <h1>Go On <br>Your First Date.</h1>
            <p>Connect With People You Know But Haven't Met Yet</p>
            <button type="button" class="btn">Know More Here</button>
        </div>
        <a href="#" class="download">DOWNLOAD THE APP <img src=""></a>

    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>
