<?php 
  $_SESSION['error'] = '';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Hugo 0.98.0">
    <title><?php echo $titulo ?></title>

    <link href="<?php echo APP_IMG ?>icon.ico" rel="shortcut icon" type="image/x-icon"/><link href="<?php echo APP_IMG ?>icon.ico" rel="apple-touch-icon"/>
        <!-- Font Awesome -->
        <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        rel="stylesheet"
        />
        <!-- Google Fonts -->
        <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        rel="stylesheet"
        />
        <!-- MDB -->
        <link
        href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css"
        rel="stylesheet"
        />
    <link href="<?php echo APP_CSS ?>default/defalpha.css" rel="stylesheet">
    <?php require ('./../App/Integrations.php'); ?> 
</head>
<header>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light " style="background-color:#ffffff;box-shadow:none">
  <!-- Container wrapper -->
  <div class="container-fluid">

    <!-- Navbar brand -->
    <a class="navbar-brand" href="./"><img src="<?php echo APP_IMG ?>logo/logoaffecy-txt.png" class="logo"></a>

    <!-- Toggle button -->
    <button class="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Collapsible wrapper -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <!-- Link -->
        <li class="nav-item">        
        </li>
      </ul>

      <!-- Links -->
      <ul class="navbar-nav d-flex flex-row me-1">
        <li class="nav-item me-3 me-lg-0">
            <a class="nav-link link-menu" href="#features">About</a>
        </li>
        <li class="nav-item me-3 me-lg-0">
            <a class="nav-link btn-menu" href="#formsub">SIGN UP</a>
        </li>
      </ul>

    </div>
  </div>
  <!-- Container wrapper -->
</nav>
<!-- Navbar -->
</header>

<body>
<div class="container">

<!-- ================== Headline ================= -->

    <section class="headline">
        <div class="row justify-content-center">
              
            <div class="col-md-6">
        
                <h1 style="font-weight:700">Is this crush for you?</h1>
                <p class="p-subtitle" style="font-size:25px;margin-bottom:30px">
                  Life is short. That's why your date needs to be so special.
                </p>
                <p class="p-subtitle">
                We exist to help you with an intelligence that, through your crush's answers, we calculate the level of combination between you.
                </p>
          <p class="p-btn">
            <a class="btn-default" href="#formsub">Register for free</a>
                </p>
        
            </div>
            <div class="col-md-6 text-center">
                <img src="<?php echo APP_IMG ?>site/couple-1.png" alt="Couple Affecy" title="Couple Affecy" width="80%" class="img-couple">
            </div>
              
          
        </div>
    </section>
  
<!-- ================== Features ================= -->
<section class="features" id="features">
  <div class="row justify-content-center">
  <h2>Your time and your feelings matter.</h2>

    <div class="col-md-4">
      <div class="card" style="background-color:#ffffff;">
        <div class="card-body">
          <h5 class="card-title" style="color: #fb7991"><i class="fas fa-skull-crossbones fa-1x" ></i> Fakes</h5>
          <p class="card-text">
          Don't be fooled by the pretty face. Check if it really matches.
          </p>
        </div> <!-- card-body -->
      </div><!-- card -->
    </div><!-- col-md-4 -->

    <div class="col-md-4">
      <div class="card" style="background-color:#ffffff;">
          <div class="card-body">
            <h5 class="card-title" style="color:#4dbce9"><i class="fas fa-cubes"></i> Technology</h5>
            <p class="card-text">
            Answers that help you decide on your future dating.
            </p>
          </div> <!-- card-body -->
        </div><!-- card -->
    </div>

    <div class="col-md-4">
      <div class="card" style="background-color:#ffffff;">
        <div class="card-body">
        
          <h5 class="card-title" style="color: #9aa5f0"><i class="far fa-clock "></i> Time</h5>
          <p class="card-text">
          Save your time and feeling after the match.
          </p>
        </div> <!-- card-body -->
      </div><!-- card -->
    </div>
  </div><!-- row -->

</section>
</div><!-- container -->

<div class="">
<!-- ================== Subscription ================= -->
<section class="formsub" id="formsub">
  <div class="row justify-content-center">
  <h2 class="h-title" style="text-align:center">Sign up for free</h2>
  <p class="p-subtitle" style="text-align:center">
    Save your early access and earn extra credits.<br>
    Your future dating will be much better.
  </p>
  
    <div class="col-sm-4 .offset-sm-1">
<?php
  $sub = new App\Models\SubModel();
  $sub->subscribe();
?>
      <form method="post">
        <!-- Name input -->
        <div class="form-outline mb-4">
          <input type="text" id="formname" class="form-control form-control-lg <?php echo @$_SESSION['error'] ? 'is-invalid' : '' ?>"  <?php echo @$_SESSION['error'] == 1 ? 'autofocus' : '' ?> maxlenght="100" name="sbname" required  />
          <label class="form-label" for="formname">Your name <abbr>*</abbr></label>
        </div>

        <!-- Email input -->
        <div class="form-outline mb-4">
          <input type="email" id="formemail" class="form-control form-control-lg" maxlenght="150" name="sbemail" required  />
          <label class="form-label" for="formemail">Your email address <abbr>*</abbr></label>
        </div>

        <!-- Country input -->
        <div class="form-outline mb-4">
          <input type="text" id="formacountry" class="form-control form-control-lg" maxlenght="100" name="sbcountry" required  />
          <label class="form-label" for="formacountry">Country <abbr>*</abbr></label>
        </div>

         <!-- Age input -->
         <!--<div class="form-outline mb-4">
          <input type="number" id="formacountry" class="form-control form-control-lg" maxlenght="2" name="sbage" />
          <label class="form-label" for="formacountry">Your age</label>
        </div>-->

        <input type="hidden" name="subform">

        <!-- Submit button -->
        <button type="submit" class="btn-default subs text-center">Subscribe now</button>
      </form>
    </div><!--col-6-->
  </div><!--row-->
</section><!--headline-->

<section class="textend">
  <div class="row justify-content-center">
    <div class="col-sm-8 .offset-sm-1">
      <h4 class="textend">
      Relationships are complex. People think they know what they want in their partner,
      but the truth is, the choice is not a conscious one.<br>
      Affecy will make this choice more conscious. 
      </h4>
    </div>
  </div>
</section>

<footer>
  <!-- Copyright -->
  <div class="text-left p-3 text-muted" style="font-size:12px">
      © 2022 Affecy.com - Copyright 
      
      <a href="https://facebook.com/affecy" title="Facebook Affecy" alt="Facebook Affecy"><i class="fab fa-facebook fa-2x" style="float:right"></i></a>
      <a href="https://instagram.com/affecyofficial" title="Instagram Affecy" alt="Instagram Affecy"><i class="fab fa-instagram fa-2x" style="float:right;padding-right:10px"></i></a>
  </div>
</footer>



</body>
<!-- MDB -->
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.js"
></script>
</html>