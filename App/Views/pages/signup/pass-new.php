<?php
	
    //Verifica se o usuário está logado
    if(isset($_SESSION['logado'])){
        header('location: p/perfil');
    }

    if(isset($_GET["t"])){
        $query = App\Conn::connect()->prepare("SELECT `forget` FROM `tbuser` WHERE `forget` = ?");
        $query->execute(array($_GET["t"]));

        if($query->rowCount() == 0){
          //header('location:' .PATH.'/login');
            die('Password already changed or Invalid Token.');
        }
    }
?>


<header>
  <!-- navbar -->
</header>
<body style="background:#fff">
<div class="container">

<div class="row justify-content-center">
    <img src="<?php echo APP_IMG ?>logo/logounique.png" class="logo-origin">
</div>

<!-- ================== Subscription ================= -->
<section class="formregister" id="formsub">
  <div class="row justify-content-center">
         
    <div class="col-lg-4">
    <h2 class="h-title" style="text-align:center;">Create new password</h2>
    <p class="p-subtitle" style="text-align:center;padding-bottom:15px"><br>
    </p>
           
      <div class="reg">
<?php
  App\Models\UserModel::newPass();
?>
          <form method="post" class="justify-content-center" >
            
            <!-- Pass input -->
            <div class="form-outline">
              <input type="password" id="formpass1" name="formpwd" class="form-control form-dtxt" maxlength="15"  placeholder="New password" />
              <label class="form-label" for="formpass1">New password</label>
            </div>
            <!-- ConfirmPass input -->
            <div class="form-outline mb-5">
              <input type="password" id="formpass2" name="formpwdc" class="form-control form-dtxt" maxlength="15" placeholder="Confirm new password" />
              <label class="form-label" for="formpass2">Confirm password</label>
              <small class="text-muted dlegend-2">Use 6 or more characters with a mix of letters, numbers & symbols.</small>
            </div>
            
              
            <input type="hidden" name="newpass" value="yes">
            
            <div class="form-outline">
                <input type="checkbox" name="showpass" id="flexcheck" onclick="showPass()" class="form-check-input">
                <label class="form-check-label" for="flexcheck"><small class="text-muted ">Show password</small></label>
            </div>
            <!-- Submit button -->
            <div class="form-outline mb-4">
                <div class="row justify-content-center">
                  <input type="submit" class="btn-login text-center" name="action" value="Reset Password">
                </div>
            </div>

          </form>
          <hr style="color:#cccccc;width:85%">
        </div><!-- reg -->
      
    </div><!--col-4-->

  </div><!--row-->
</section><!--headline-->


</div><!-- container -->