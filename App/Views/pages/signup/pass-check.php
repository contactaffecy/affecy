

<header>
  <!-- navbar -->
</header>
<body style="background:#fff">
<div class="container">

<div class="row justify-content-center">
<span class=" text-center"><i class="fa-solid fa-envelope-open-text fa-xl box-img "></i></span>
</div>

<!-- ================== Subscription ================= -->
<section class="formregister" id="formsub">
  <div class="row justify-content-center">
         
    <div class="col-lg-4">
    <h2 class="h-title" style="text-align:center">Check your mail</h2>
    <p class="p-subtitle" style="text-align:center;padding-bottom:15px">We have sent a password recover<br>
    instructions to your email.</p>
           
      <div class="reg">
                           
         <div class="form-outline mb-4">
            <div class="row justify-content-center">
              <a href="#" class="btn-login text-center">Open email</a>
            </div>
         </div>
   
         
          <hr style="color:#cccccc;width:85%">
          <p align="center" class="complement">Did not receive the email? Check your spam filter, or <br>
          <a href="./passRecovery" class="import"> try another email address.</a></p>
    
        </div><!-- reg -->
      
    </div><!--col-4-->

  </div><!--row-->
</section><!--headline-->


</div><!-- container -->