

<header>
  <!-- navbar -->
</header>
<body style="background:#fff">
<div class="container">

<div class="row justify-content-center">
    <img src="<?php echo APP_IMG ?>logo/logounique.png" class="logo-origin">
</div>

<!-- ================== Subscription ================= -->
<section class="formregister" id="formsub">
  <div class="row justify-content-center">
         
    <div class="col-lg-4">
    <h2 class="h-title" style="text-align:left;padding-left:20px;">Reset password</h2>
    <p class="p-subtitle" style="text-align:left;padding-bottom:15px;padding-left:20px">Enter the email associated with your account<br>
    and we'll send an email with instructions to reset your password.</p>
           
      <div class="reg">
<?php
  $reg = new App\Models\UserModel();
  $reg->forget();
?>
          <form method="post" class="justify-content-center">
             <!-- Email input -->
            <div class="form-outline text-left">
                <label style="float:left;">Email</label><br>
                <input type="email" id="formemail" class="form-dtxt" maxlength="150" name="uemail" placeholder="My email..." required />
            </div>
              

          <input type="hidden" name="passrecover" value="yes">
                      
            <!-- Submit button -->
            <div class="form-outline mb-4">
                <div class="row justify-content-center">
                  <input type="submit" class="btn-login text-center" name="action" value="Send Reset Password">
                </div>
            </div>
          </form>
          <hr style="color:#cccccc;width:85%">
        </div><!-- reg -->
      
    </div><!--col-4-->

  </div><!--row-->
</section><!--headline-->


</div><!-- container -->