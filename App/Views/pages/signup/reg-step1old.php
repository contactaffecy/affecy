<?php
  

 

?>
<body class="backgrad">
<div class="container">

<div class="row justify-content-center">
    <img src="<?php echo APP_IMG ?>logo/logowhite-txt.png" class="logo-origin">
</div>

<!-- ================== Subscription ================= -->
<section class="formregister" id="formsub">
  <div class="row justify-content-center">
         
    <div class="col-lg-4">
      <form method="post" class="reg">
      <h4 class="h-title" style="text-align:left;">Your registration is created.</h4>
      <p class="p-subtitle" style="text-align:left;padding-bottom:15px;font-size:14px;line-height:1rem">Complete with some more information.</p>
      
        <!-- Name input -->
        <div class="form-outline mb-4">
          <input type="text" id="forname" class="form-control form-control-lg dtxt" maxlenght="150" name="ufname" placeholder="My first name..." />
        </div>
        <!-- Nick input -->
        <div class="form-outline  mb-4">
          <input type="text" id="fornick" class="form-control form-control-lg dtxt" maxlenght="20" name="unick" placeholder="My nick is..." />
        </div>
        
        <!-- Birthday input -->
        <div class="form-outline mb-4">
          <div class="form-outline">
            <label style="padding-left:10px">My birthday is...</label>
              <input placeholder="My birthday is..." type="date" id="forubday" name="ubday" class="form-control form-control-lg dtxt">         
          </div>
          <small class="text-muted dlegend">Your age will not be published.</small>
        </div>
       
        <input type="hidden" name="register" value="yes">
                   
        <!-- Submit button -->
        <div class="row justify-content-center">
          <input type="submit" class="btn-default bgradient text-center" name="action" value="Done">
        </div>

      </form>
    </div><!--col-4-->
    
  </div><!--row-->
</section><!--headline-->


</div><!-- container -->

