
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Affecy 1.0.0">
    <title>Create your account in Affecy</title>

    <link href="<?php echo APP_IMG ?>icon.ico" rel="shortcut icon" type="image/x-icon"/><link href="<?php echo APP_IMG ?>icon.ico" rel="apple-touch-icon"/>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css" rel="stylesheet" />
    <link href="<?php echo APP_CSS ?>default/debeta_1.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.8/jquery.mask.js"></script>
  </head>
<header>
  <!-- navbar -->
</header>
<body style="background:#fff">
<div class="c-loader"></div>
<div class="container">
<?php
 if(@$_SESSION['access'] == 0 && @$_SESSION['initial'] == 0){
  $alert = App\Controllers\AlertsController::alert('info', 'Only this step is left to complete your registration.');
  $text = '';
}

?>
<div class="row justify-content-center">
    <img src="<?php echo APP_IMG ?>logo/logounique.png" class="logo-origin">
</div>

<!-- ================== Subscription ================= -->
<section class="formregister" id="formsub">
  <div class="row justify-content-center">
         
    <div class="col-auto">
    <h2 class="h-title" style="text-align:center">Your registration is created! <img src="<?php echo PATH ?>/Public/img/site/1f389.png" width="30px"></h2>
    <p class="p-subtitle" style="text-align:center;padding-bottom:15px">Complete with some more information.</p>
           
      <div class="reg">
<?php
  $reg = new App\Models\RegisterModel();
  $reg->registerStep();
?>
          <form method="post" class="justify-content-center">
             <!-- Email input -->
            <div class="form-outline mb-3">
              <input type="text" id="forname" class="form-dtxt form-control active <?php echo @$_SESSION['reg-name-erro'] == 1 ? 'is-invalid' : '' ?>" maxlength="30" name="ufname" required />
              <label class="form-label" for="forname">My first name*</label>
            </div>

            <!-- Nick input -->
            <div class="form-outline mb-4">
              <input type="text" id="fornick" class="form-dtxt form-control active<?php echo @$_SESSION['reg-nick-erro'] == 1 ? 'is-invalid' : '' ?>" maxlength="10" name="unick" required />
              <label class="form-label" for="fornick">My nickname*</label>
              <small class="text-muted dlegend">Use a nickname with a min. of 3 chars.</small>
            </div>

            <!-- Birthday input -->
            <div class="form-outline mb-5">
                  <input placeholder="My birthday*" type="date" id="forubday" name="ubday" class="form-dtxt active form-control" required>         
                  <label class="form-label" for="forname">My birthday*</label>
                  <small class="text-muted dlegend">Your age will not be published.</small>
              </div>
              

          <input type="hidden" name="reg-continue" value="yes">
                      
            <!-- Submit button -->
            <div class="form-outline mb-4">
                <div class="row justify-content-center">
                  <button type="submit" class="btn-login text-center" name="action">Done</button>
                </div>
            </div>
          </form>
          <hr style="color:#cccccc;width:85%">
        </div><!-- reg -->
      
    </div><!--col-4-->

  </div><!--row-->
</section><!--headline-->


</div><!-- container -->
<script>
  $(document).ready(function(){
    let txty = document.querySelector('input[name="ubday"]');
    txty.style.opacity = '1';
  });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="<?php echo PATH ?>/Public/js/scripts.js"></script>