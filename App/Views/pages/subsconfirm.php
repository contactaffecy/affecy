<?php 
  unset($_SESSION['confirmation']);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Hugo 0.98.0">
    <title>Affecy - Successful subscription</title>

    <link href="https://uploads-ssl.webflow.com/6296d203d2a1456a68f7a5c9/629c4a0a33cf8026734d05c3_logo_4_affecy(simbol).png" rel="shortcut icon" type="image/x-icon"/><link href="https://uploads-ssl.webflow.com/6296d203d2a1456a68f7a5c9/629c4a254ba1c5d02ed9b6e7_logo_4_affecy_256.png" rel="apple-touch-icon"/>
        <!-- Font Awesome -->
        <link
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
        rel="stylesheet"
        />
        <!-- Google Fonts -->
        <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        rel="stylesheet"
        />
        <!-- MDB -->
        <link
        href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css"
        rel="stylesheet"
        />
    <link href="<?php echo APP_CSS ?>default/defalpha.css" rel="stylesheet">
   
    <?php require ('./../App/Integrations.php'); ?> 
</head>
<header>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light " style="background-color:#ffffff;box-shadow:none">
  <!-- Container wrapper -->
  <div class="container-fluid">

    <!-- Navbar brand -->
    <a class="navbar-brand" href="<?= PATH ?>"><img src="<?php echo APP_IMG ?>logo/logoaffecy-txt.png" class="logo"></a>

  </div>
  <!-- Container wrapper -->
</nav>
<!-- Navbar -->
</header>

<body>
<div class="cssload-dots">
	<div class="cssload-dot"></div>
	<div class="cssload-dot"></div>
	<div class="cssload-dot"></div>
	<div class="cssload-dot"></div>
	<div class="cssload-dot"></div>
</div>

<svg version="1.1" xmlns="http://www.w3.org/2000/svg">
	<defs>
		<filter id="goo">
			<feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="12" ></feGaussianBlur>
			<feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7" result="goo" ></feColorMatrix>
			<!--<feBlend in2="goo" in="SourceGraphic" result="mix" ></feBlend>-->
		</filter>
	</defs>
</svg>
<div class="container">

<!-- ================== Headline ================= -->
<section class="headline">
    <div class="row justify-content-center">
          
        <div class="col-md-6">
            <h1>Your registration was successful!</h1>
            <p class="p-subtitle" style="font-size:25px;margin-bottom:30px">
              Your diamonds have been saved.
            </p>
           
            <p class="p-btn">
              <a class="btn-menu confirm" href="https://instagram.com/affecyofficial"><i class="fab fa-instagram" style="padding-right:14px"></i> Follow us on instagram</a>
            </p>
		
        </div>
        <div class="col-md-6 text-center">
            <img src="<?php echo APP_IMG ?>site/confirm.png" alt="Confirm Affecy" title="Confirm Affecy" width="60%" class="img-couple">
        </div>
           
       
    </div>
</section>


<section class="textend">
  <div class="row justify-content-center">
    <div class="col-sm-8 .offset-sm-1">
      <h4 class="p-textend">
      Your email has been registered in our database. <br>
     
      </h4>
      <br>
      <h4 class="textend">
      Follow us on social networks. <br>Our technology will soon be available to help you with dating.
      </h4>
    </div>
  </div>
</section>


<footer>
  <!-- Copyright -->
  <div class="text-left p-3 text-muted" style="font-size:12px">
      © 2022 Affecy.com - Copyright 
      <a href="https://facebook.com/affecy" title="Facebook Affecy" alt="Facebook Affecy"><i class="fab fa-facebook fa-2x" style="float:right"></i></a>
      <a href="https://instagram.com/affecyofficial" title="Instagram Affecy" alt="Instagram Affecy"><i class="fab fa-instagram fa-2x" style="float:right;padding-right:10px"></i></a>
  </div>
</footer>


</div><!-- container -->
</body>
<!-- MDB -->
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.js"
></script>
</html>