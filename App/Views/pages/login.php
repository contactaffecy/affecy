<?php
	
    //Verifica se o usuário está logado
    if(isset($_SESSION['logado'])){
        header('location: p/home');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Affecy 1.0.0">
    <title>Login | Affecy</title>

    <link href="<?php echo APP_IMG ?>icon.ico" rel="shortcut icon" type="image/x-icon"/><link href="<?php echo APP_IMG ?>icon.ico" rel="apple-touch-icon"/>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css" rel="stylesheet" />
    <link href="<?php echo APP_CSS ?>default/debeta_1.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<header>
  <!-- navbar -->
</header>
<body style="background:#fff">
<div class="container">

<div class="row justify-content-center">
    <img src="<?php echo APP_IMG ?>logo/logounique.png" class="logo-origin">
</div>

<!-- ================== Subscription ================= -->
<section class="formregister" id="formsub">
  <div class="row justify-content-center">
         
    <div class="col-lg-4">
    <h2 class="h-title" style="text-align:center">Hello!</h2>
    <p class="p-subtitle" style="text-align:center;padding-bottom:15px">I was thinking of you ;)</p>
           
      <div class="reg">
<?php
  $reg = new App\Models\UserModel();
  $reg->login();
?>
          <form method="post" class="justify-content-center">
             <!-- Email input -->
            <div class="form-outline">
              <input type="email" id="formemail" class="form-dtxt" maxlength="150" name="uemail" placeholder="My email..." />
            </div>

            <!-- Pass input -->
            <div class="form-outline mb-4">
              <input type="password" id="formpass1" class="form-dtxt" maxlength="15" name="ucript" placeholder="My password" autocomplete/>
              <small class="dlegend import"><a href="<?php echo PATH ?>/passRecovery" class="import">Forgot Password?</a></small>
            </div>

          <input type="hidden" name="login" value="yes">
            
            <!-- Submit button -->
            <div class="form-outline mb-4">
              <input type="submit" class="btn-login text-center" style="width:100%" name="action" value="Login">
            </div>
          </form>
          <hr style="color:#cccccc;width:85%">
          <p align="center" class="complement">Dont't have an account?<a href="<?php echo PATH ?>/register" title="Register Affecy" alt="Register Affecy" class="import"> Register</a></p>
      </div><!-- reg -->
      
    </div><!--col-4-->

  </div><!--row-->
</section><!--headline-->


</div><!-- container -->