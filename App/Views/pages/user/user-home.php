
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8 offset-md-3">
        <section id="welcome">
            <span>Hello, <?= ucfirst($info['user_firstname']) ?> <br> 
                How about sending a message to crush today?
            </span>
        </section>

        <section id="cards">
            <div class="card-esp" style="background-image: url('<?= PATH ?>/Public/img/prof/prof-card.png')">
                <span>You have a new combo answer!</span>
                <p>Check out the result and who knows this could be your next love.</p>    
            </div>
        </section>

        <section id="cards">
            <div class="card-esp" style="background-image: url('<?= PATH ?>/Public/img/prof/prof-card-pb.png')">
                <span>For now you have no match results.</span>    
                <p>Submit your test for your next date.</p>
            </div>
        </section>
        </div><!--col-->
    </div><!--row-->
        <a href="#" class="btn-test"><i class="fa-solid fa-paper-plane" style="margin-top:13px"></i></a>
</div><!--container -->