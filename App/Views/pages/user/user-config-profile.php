

<body style="background:#f5f5f5">
<div class="container-fluid">

<!-- ================== Form ================= -->
<section class="formregister" id="formsub" style="background-color:#f5f5f5">
  <div class="row justify-content-center">
         
    <div class="col-md-6">
    <a href="./settings" class="linkmenu">
      <span class="linkmenu" style="float:left"><i class="fa-solid fa-angle-left" style="padding-right:4px"></i> Voltar</span>
    </a>
    <h2 class="h-title" style="text-align:center;"> Edit profile</h2>
        <p class="p-subtitle" style="text-align:center;padding-bottom:15px"><br>
        </p>
           
        <div class="form-grid">
<?php
  App\Models\UserModel::editProfile();
?>
        <form method="post" style="text-align:left;">

          <!-- Email -->
            <div class="form-outline mb-4">
                <label class="form">Email: <span style="color:#9aa5ed;font-weight:700"><?= $info['user_email'] ?></span></label>
            </div>
            
            <!-- first e last name -->
            <div class="row g-2 mb-3">
                <div class="col-sm-4">
                    <label class="form" for="forfname">First Name<sup style="color:red">*</sup></label>
                    <input type="text" id="forfname" name="forfname" class=" form-dtxt form-control" maxlength="40" value="<?= $info['user_firstname'] ?>" required />
                </div>
            
                <div class="col-sm-8">
                    <label class="form" for="forlname">Last Name</label>
                    <input type="text" id="forlname" name="forlname" class="form-control form-dtxt" maxlength="40" value="<?= $info['user_lastname'] ?>" />              
                </div>
            </div><!-- row -->

            <!-- Nickname input -->
            <div class="form-outline mb-4">
                <label class="form" for="fornick">Nickname<sup style="color:red">*</sup></label>
                <input type="text" id="fornick" name="fornick" class="form-control form-dtxt form-disabled" value="<?= $info['user_nick'] ?>" disabled />
            </div>

            <!-- Birthday input -->
            <div class="form mb-5">
                <label class="form" for="forname">My birthday<sup style="color:red">*</sup></label>
                <input placeholder="My birthday*" type="date" id="forubday" name="ubday" class="form-dtxt active form-control" value="<?= $info['user_bday'] ?>" required>         
                <small class="text-muted dlegend" style="float:left">Your age will not be published.</small>
            </div>
            
            <input type="hidden" name="update" value="yes">
           
            <!-- Submit button -->
            <div class="form mb-4">
                <div class="row justify-content-center">
                  <input type="submit" class="btn-login text-center" name="action" value="Save">
                </div>
            </div>

          </form>
         
        </div><!-- reg -->
      
    </div><!--col-4-->

  </div><!--row-->
</section><!--headline-->


</div><!-- container -->