
<?php 


$emailError = @$_SESSION['reg-email-erro'];
$emailMsg = @$_SESSION['reg-email-msg'];

$passError = @$_SESSION['reg-pass-erro'];
$passMsg = @$_SESSION['reg-pass-msg'];

  
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="The next level of the match">
    <meta name="author" content="Affecy.com">
    <meta name="generator" content="Affecy 1.0.0">
    <title>Create your account in Affecy</title>

    <link href="<?php echo APP_IMG ?>icon.ico" rel="shortcut icon" type="image/x-icon"/><link href="<?php echo APP_IMG ?>icon.ico" rel="apple-touch-icon"/>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="<?php echo APP_CSS ?>default/debeta_1.css" rel="stylesheet">      

  </head>
<header>
  <!-- navbar -->
</header>
<body style="background:#fff">
<div class="c-loader"></div>
<div class="container">

<div class="row justify-content-center">
    <img src="<?php echo APP_IMG ?>logo/logounique.png" class="logo-origin">
</div>

<!-- ================== Subscription ================= -->
<section class="formregister" id="formsub">
  <div class="row justify-content-center">
         
    <div class="col-auto">
    <h2 class="h-title" style="text-align:center">Sign Up</h2>
    <p class="p-subtitle" style="text-align:center;padding-bottom:15px">It's quick, easy and free.</p>
           
      <div class="reg">
<?php
  $reg = new App\Models\RegisterModel();
  $reg->register();

?>
          <form method="post" class="justify-content-center needs-validation" id="regform" novalidate>
             <!-- Email input -->
            <div class="form-outline p3">
              <input type="email" id="formemail" class="form-dtxt form-control <?php echo $emailError == 1 ? 'is-invalid' : '' ?>" maxlength="150" name="uemail" placeholder="My email..." />
            </div>
             
            <!-- Pass input -->
            <div class="form-outline mb-5">
              <input type="password" id="formpass1" class="form-dtxt form-control <?php echo (@$_SESSION['reg-pass-erro'] == 1 ? 'is-invalid' : '') ?>" maxlength="15" name="ucript" placeholder="My password">
              <span class="fa-solid fa-eye"></span>
              <small class="text-muted dlegend-2">Use 6 or more characters with a mix of letters, numbers & symbols.</small>
            </div>
                     
            <input type="hidden" name="register" value="yes">
                      
            <!-- Submit button -->
            <div class="form-outline">
              <div class="row justify-content-center">
                <button type="submit" class="btn-login btn-reg text-center" disabled="disabled">Continue</button>
                <small class="text-muted dlegend">By clicking Sign Up, you agree to our Terms, Data Policy and Cookies Policy.</small>
              </div>
            </div>
           
          </form>
          <hr style="color:#cccccc;width:85%">
          <p align="center" class="complement">Have an account? <a href="<?php echo PATH ?>/login" title="Login Affecy" alt="Login Affecy" class="import"> Log in</a></p>
      </div><!-- reg -->
      
    </div><!--col-4-->

  </div><!--row-->
</section><!--headline-->


</div><!-- container -->

<script>
    let btn = document.querySelector('.fa-eye');
    btn.addEventListener('click', function() {
      let input = document.querySelector('#formpass1');
      if(input.getAttribute('type') == 'password') {
          input.setAttribute('type', 'text');
      } else {
          input.setAttribute('type', 'password');
      }
  });
</script>
    
  
   <script>
      // Self-executing function
      (function() {
          'use strict';
          window.addEventListener('load', function() {
              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.getElementsByClassName('needs-validation');
              
              // Loop over them and prevent submission
              var validation = Array.prototype.filter.call(forms, function(form) {
                  form.addEventListener('submit', function(event) {
                      if (form.checkValidity() === false) {
                          event.preventDefault();
                          event.stopPropagation();
                      }
                      form.classList.add('was-validated');
                  }, false);
              });
          }, false);
      })();
    </script>

        <script>
window.addEventListener("load", function() {
    var fadeContainer = document.querySelector("#c-loader");
  
  setTimeout(function() {
      
        fadeContainer.style.display = "none";
    
  }, 1000);
});
          </script>