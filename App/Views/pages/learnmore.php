
<body style="height:auto">
<div class="container" style="margin-top:45px">
       
    <section class="headline" style="padding:50px 20px">
        <div class="container">
            <h2 class="headline-h h-bl">Make your dating decision easier with technology.</h2>
            <div class="space"></div> 
            <div class="row justify-content-center par-4" style="display:-webkit-flex;
        -webkit-align-items: center;display:flex;align-items: center;">
                <div class="col-md-6">
                   
                    <div class="how">
                        <span class="how-number">1</span>
                        <span class="how-title w5">Secret answers</span>
                        <span class="how-desc">Create the answers and choose the level of happiness in each one.</span>
                    </div>

                    <div class="how">
                        <span class="how-number">2</span>
                        <span class="how-title w5">Share with crush</span>
                        <span class="how-desc">Send your Affecy smart form to crush.</span>
                    </div>

                    <div class="how">
                        <span class="how-number">3</span>
                        <span class="how-title w5">Magic happens</span>
                        <span class="how-desc">Our algorithm will work with some variables and show the degree of combination between you.</span>
                    </div>

                    <div class="how">
                        <span class="how-number">4</span>
                        <span class="how-title w5">Match rate</span>
                        <span class="how-desc">View the match rate and responses that most closely match you.</span>
                    </div>
                    <div style="text-align:center">
                        <a href="<?= PATH ?>/#subs" alt="Subscription" title="Subscription" class="btn-login text-center">Subscription now</a>
                    </div>
                </div><!-- col -->
                    
                <div class="col-md-6">
                    <img src="<?php echo APP_IMG ?>site/affecy_card1.png" alt="Card Affecy" title="Card Affecy" class="img-site-card">
                </div><!-- col -->
                
            </div><!-- row -->
        </div>
    </section>
</div><!--container-->


<script>
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('.nav').addClass('affix');
            console.log("OK");
        } else {
            $('.nav').removeClass('affix');
        }
    });
</script>





    
