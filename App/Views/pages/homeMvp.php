
<body style="height:auto">
<div class="cssload-dots">
	<div class="cssload-dot"></div>
	<div class="cssload-dot"></div>
	<div class="cssload-dot"></div>
	<div class="cssload-dot"></div>
	<div class="cssload-dot"></div>
</div>


<div class="container" style="margin-top:45px">
    
    <section class="headline">
        
        <div class="row justify-content-center">
            <div class="col-md-4">    
                <h1 class="headline-h">Feelings and time are important.</h1>
                <p class="sub-desc" style="margin:20px 0;color:#999">So, test the combination with our algorithm before moving on to the date or relationship. See if it really is for you.</p>
            </div>
            <div class="col-md-6">
                <img src="<?php echo APP_IMG ?>site/couple-3.png" style="width:80%">
            </div>
            <div class="space"></div>                     
        </div><!-- row -->
    
        <div class="row justify-content-center">
           <div class="col-sm-6">
           <?php
    $sub = new App\Models\SubModel();
    $sub->subscribe();
?>         
                <form method="post" class="row g-1 align-items-center needs-validation" id="subs" novalidate>
                    <span class="p-subtitle" style="text-align:left;color:#4f4f4f">
                    Get early access and improve your match:
                    </span>
                    <div class="col-sm-3">
                        <label class="visually-hidden" for="inname">Name</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="sbname" id="formName" placeholder="Name" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <label class="visually-hidden" for="inemail">Email</label>
                        <div class="input-group">
                            <input type="email" class="form-control" name="sbemail" id="formEmail" placeholder="Email address" />
                        </div>
                        <input type="hidden" class="form-control" name="subform" value="form" />
                    </div>
                    <div class="col-sm">
                        <button type="submit" class="btn-login bgradient text-center" id="btnSub" style="padding:8px 0px;border-radius:8px;width:100%" disabled="disabled">Subscription</button>
                    </div>
                    <span class="p-subtitle" style="text-align:left;margin-top:-1.5rem;color:#4f4f4f;font-size:12px">
                        <img src="<?php echo APP_IMG ?>site/diamond.png" style="width:20px"> Get some diamonds by signing up now.
                    </span>
                </form>


               
            </div><!-- col -->
        </div><!-- row -->
    
    </section>  
    <div class="space"></div> 
</div><!-- container -->

<section class="headline-s howork" id="features">
    <div class="container">
        <h2 class="headline-h h-bl">The next level of the match.</h2>
       
            <!-- ================== Features ================= -->
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="card" style="background-color:#ffffff;">
                        <div class="card-body w5">
                            <img src="<?php echo APP_IMG ?>/site/icon1.png">
                            <h2 class="card-title"> <span class="card-span">Decrease the fakes.</span>
                                Don't be fooled by the pretty face. Check if it really matches.
                            </h2>
                        </div> <!-- card-body -->
                    </div><!-- card -->
                </div><!-- col-md-4 -->

                <div class="col-md-3">
                    <div class="card" style="background-color:#ffffff;">
                        <div class="card-body">
                            <img src="<?php echo APP_IMG ?>/site/icon3.png" >
                            <h2 class="card-title"><span class="card-span">Technology.</span>
                                Answers that help you decide on your future dating.
                            </h2>
                        </div> <!-- card-body -->
                    </div><!-- card -->
                </div>

                <div class="col-md-3">
                    <div class="card" style="background-color:#ffffff;">
                        <div class="card-body">
                            <img src="<?php echo APP_IMG ?>/site/icon6.png">
                            <h2 class="card-title"><span class="card-span">Time.</span>
                                Save your time and feeling after the match.
                            </h2>
                        </div> <!-- card-body -->
                    </div><!-- card -->
                </div>

                <div class="col-md-3">
                    <div class="card" style="background-color:#ffffff;">
                        <div class="card-body">
                            <img src="<?php echo APP_IMG ?>/site/icon7.png">
                            <h2 class="card-title"><span class="card-span">Self-reliance</span>
                                Find out if there really is a match and don't lose your confidence.
                            </h2>
                        </div> <!-- card-body -->
                    </div><!-- card -->
                </div>
            </div><!-- row -->

        <div class="space"></div>
    </div><!--container-->
</section>



    <hr style="text-align:center;color:#f5f5f5;">

    <section class="textend">
        <div class="row justify-content-center">
            <div class="col-sm-8 .offset-sm-1">
                <h4 class="textend">
                Relationships are complex. People think they know what they want in their partner,
                but the truth is, the choice is not a conscious one.<br>
                Affecy make this choice more conscious. 
                </h4>
            </div>
        </div>
    </section>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs>
        <filter id="goo">
            <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="12" ></feGaussianBlur>
            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7" result="goo" ></feColorMatrix>
            <!--<feBlend in2="goo" in="SourceGraphic" result="mix" ></feBlend>-->
        </filter>
    </defs>
</svg>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('.nav').addClass('affix');
            console.log("OK");
        } else {
            $('.nav').removeClass('affix');
        }
    });
</script>
<script>
    $(document).ready(function() {
        $('.cssload-dots').hide();
        $('#btnSub').click(
                function() {

                    $('.cssload-dots').show();

                }
            );
        }
    );
 </script>
<script>
    // Self-executing function
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>




    
