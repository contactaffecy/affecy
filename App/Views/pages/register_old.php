<?php
  require './../App/Autoload.php';

  $_SESSION['regs'] = 1;

  if(@$_GET['step'] == '23'){
    require ('./../App/Views/pages/signup/reg-step1.php');
  }


?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="The next level of the match">
  <meta name="author" content="Affecy.com">
  <meta name="generator" content="Affecy 1.0.0">
  <title><?php echo $titulo ?></title>

  <link href="<?php echo APP_IMG ?>icon.ico" rel="shortcut icon" type="image/x-icon"/><link href="<?php echo APP_IMG ?>icon.ico" rel="apple-touch-icon"/>
  <!-- Font Awesome -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
  <!-- MDB -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/4.2.0/mdb.min.css" rel="stylesheet" />
  <link href="<?php echo APP_CSS ?>default/defalpha.css" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<header>
<!-- navbar -->
</header>
<body class="backgrad">
<div class="container">

<div class="row justify-content-center">
    <img src="<?php echo APP_IMG ?>logo/logowhite-txt.png" class="logo-origin">
</div>

<!-- ================== Subscription ================= -->
<section class="formregister" id="formsub">
  <div class="row justify-content-center">
         
    <div class="col-lg-4">
      <div class="reg">
          <form method="post" action="?step=23">
          <h2 class="h-title" style="text-align:center">Sign Up</h2>
          <p class="p-subtitle" style="text-align:center;padding-bottom:15px">It’s quick, easy and free.</p>
            <!-- Email input -->
            <div class="form-outline mb-4">
              <input type="email" id="formemail" class="form-control form-control-lg dtxt" maxlenght="150" name="uemail" placeholder="My email..." />
            </div>

            <!-- Pass input -->
            <div class="form-outline mb-4">
              <input type="password" id="formpass1" class="form-control form-control-lg dtxt" maxlenght="15" name="ucript" placeholder="My password" autocomplete/>
            </div>

            <!-- Pass input confirm -->
            <div class="form-outline mb-4">
              <input type="password" id="formpass2" class="form-control form-control-lg dtxt" maxlenght="15" name="ucriptconfirm" placeholder="Confirm password" autocomplete="off"/>
            </div>

          <input type="hidden" name="register" value="yes">
              <p>
                <small class="text-muted dlegend">By clicking Sign Up, you agree to our Terms, Data Policy and Cookies Policy.</small>
              </p>
            <!-- Submit button -->
            <div class="row justify-content-center">
              <input type="submit" class="btn-default bsignup text-center" name="action" value="Continue">
            </div>
          </form>
          <hr>
          <p align="center" class="font-aff"><strong>Have an account? Log in <a href="<?php echo PATH ?>/login" title="Login Affecy" alt="Login Affecy"><u>here</u></a>.</strong></p>
      </div><!-- reg -->
      
    </div><!--col-4-->

  </div><!--row-->
</section><!--headline-->


</div><!-- container -->

