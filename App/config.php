<?php
session_start();
date_default_timezone_set('America/Sao_Paulo');
define('TODAY', date('Y-m-d H:i:s'));

//Verifica periodo
$mes = new DateTime();
$mes = $mes->format('m');

$ano = new DateTime();
$ano = $ano->format('y');

define('MES', $mes);
define('ANO', $ano);

//Configurações da app a

//Interruptores
define('GOLIVE',1); // 0 - site nao liberado 1 - site liberado
define('APP_ON',1); // 0 - site offline - 1 - site online
define('APP_OFF_TIME','2h de 22/02/21'); //tempo offline
define('APP_OFF_JUST','Atualização da plataforma.'); //tempo offline

define('MODO_TESTE','ON'); // ON - Nao carrega Google Analytics

//Diretórios LOCALHOST

define('APP',dirname(__FILE__));
define('PATH','http://localhost/affecy-beta/affecy');
define('URL','http://localhost/affecy-beta/affecy/');
//define('URL','https://horky.com.br/');
define('LOCAL',$_SERVER['DOCUMENT_ROOT'].'/horky/horky-brasil');
define('PAGES', PATH.'/app/views/pages/');
define('PAGE_PROFILE',PATH.'/app/views/pages/profile');
define('BASE_AVATAR','\public\img\profile\avatar');
define('BASE_WORK','\public\img\profile');

//Conexao com o banco de dados
define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASS','');
define('DB_NAME','u671072896_bdaffe');

//Versao e outros diretorios

define('APP_NAME','Affecy Networking');
define('APP_VERSAO','1.0.0');
define('VERSAO','2022');

define('LOGO_TEXT',PATH.'/Public/img/logo/logo_affecy.png');
define('APP_CSS',PATH.'/Public/css/');
define('APP_IMG',PATH.'/Public/img/');
define('APP_CONTROL',PATH.'/app/controllers/');
define('APP_PATH_PAINEL', PAGES.'painel/');
define('BASE_DIR_PAINEL',__DIR__.'/views/pages/painel/');

define('HOME','homeMvp.php');


//Configurações de EMAIL

define('EMAIL_USUARIO','contato@horky.com.br');
define('EMAIL_SENHA','Jesusteamo@270920');
define('EMAIL_PORTA','587');
define('EMAIL_HOST','smtp.hostinger.com.br');
define('EMAIL_SMTP','');
define('EMAIL_MAILER','smtp');
define('EMAIL_CHARSET','UTF-8');
define('EMAIL_FROM','Contato Affecy');

define('EMAIL_ADM', 'contactaffecy@gmail.com'); //email ADM

//Google

define('GTAG','G-C2FK1V1YE0'); 