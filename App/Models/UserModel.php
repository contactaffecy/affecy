<?php

    namespace App\Models;

    use App\Controllers\AlertsController;
    use App\Controllers\ValidateController;
    use App\Controllers\MsgController;
    use App\Controllers\TokenController;
    use App\Utility;
    use App\Bcrypt;
    use App\Conn;
    use App\Controllers\EmailController;
    

    class UserModel {
        
        public static function emailExist($email){
            $pdo = \App\Conn::connect();
            $verify = $pdo->prepare("SELECT user_email FROM tbuser WHERE user_email = ?");
            $verify->execute(array($email));

            if($verify->rowCount() == 1){
                //Email existe
                return true;
            }else{
                return false;
            }
        }

        public static function getUserId($campo, $dado){
            $sql = Conn::connect()->prepare("SELECT * FROM `tbuser` WHERE `$campo` = ?");
            $sql->execute(array($dado));
            return $sql->fetch();
        }

        public static function login() {
            

            if(isset($_SESSION['logado']) == 1){
                //renderiza a home do usuário
                header("Location: ".PATH.'/p/home');

            }else{
                
                //renderiza para logar conta.
                if(isset($_POST['login'])){

                    $login = filter_input(INPUT_POST, 'login', FILTER_UNSAFE_RAW);
                
                //if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                    
                    $email = filter_input(INPUT_POST, 'uemail', FILTER_SANITIZE_EMAIL);
                    $pass = $_POST['ucript'];

                    //verificar no banco de dados
                    $verify = \App\Conn::connect()->prepare("SELECT * FROM tbuser WHERE user_email = ?");
                    $verify->execute(array($email));

                    if($verify->rowCount() == 0){
                        //não existe o usuário!
                        AlertsController::alert('danger', 'Email or password is incorrect.');
                       
                    }else{
                        $data = $verify->fetch();
                        $passBase = $data['user_pwd'];

                        if(\App\Bcrypt::check($pass, $passBase)){
                            //usuario logado com sucesso
                            $_SESSION['user-login'] = $data['user_email'];
                            $_SESSION['user-name'] = $data['user_firstname'];

                            //Registra log de acesso
                            self::logAccess($data['id']);
                            AlertsController::alert('success', 'Login successfully!');
                            /*\App\Utility::redirect(PATH);*/

                           //verifica se cadastro está completo
                                if($data['user_firstname'] == NULL){
                                    $_SESSION['access'] = 0;
                                    $_SESSION['reg-email'] = $data['user_email'];
                                    Utility::redirect(PATH.'/register/2');
                                    
                                }else{
                                    $_SESSION['access'] = 1;
                                    $_SESSION['logado'] = 1;
                                    $_SESSION['user_email'] = $data['user_email'];
                                    $_SESSION['id'] = $data['id'];
                                    header("Location: ".PATH.'/p/home');
                                }
                                                      
                        }else{
                            //senha não confere
                            AlertsController::alert('danger', 'Email or password is incorrect.');
                           
                        }
                    }
                }
                
               
            }
        }

        public static function logAccess($user){
            $data = date('Y-m-d H:i:s');
            $local = $_SERVER['REMOTE_ADDR'];
            $sql = \App\Conn::connect()->prepare("INSERT INTO `tbuser.log` (`id`, `user_id`, `log_local`,`log_day`) VALUES (null,?,?,?)");
            $sql->execute(array($user, $local, $data));
            
       }

       public function forget() {

            if(isset($_POST['passrecover']) == 'yes'){

                $email = filter_input(INPUT_POST, 'uemail', FILTER_SANITIZE_EMAIL);
                if(!$email){
                    AlertsController::alert('warning', 'Enter your registered email to recover your password.');

                }elseif(\App\Models\UserModel::emailExist($email) == false){
                    AlertsController::alert('warning','The email provided is not registered.');
                
                }else{

                    $codeForget = (md5(uniqid(rand(), true)));

                    $query = Conn::connect()->prepare("UPDATE `tbuser` SET `forget` = ? WHERE `user_email` = ?");
                    $query->execute(array($codeForget, $email));

                    $info = UserModel::getUserId('user_email', $email);
                
                    $_SESSION['forget'] = $info['id'];

                    $emailReset = new Emailcontroller;
                    if($emailReset->emailForget($info['user_firstname'], $info['user_email'], $info['forget'])){
                        echo '<div class="alert alert-info"><strong>'.$info['user_firstname'].'</strong>, in a few moments you will receive a link in the registered email to change your password.</div>';
                        echo '<a href="'.PATH.'/login">Return</a>';
                        die();
                    }

                }
            }

        }

       public static function newPass(){

            /** TESTAR RESET */
                                    
            if(isset($_GET["t"]) && isset($_POST['newpass']) == 'yes'){
                $newpass = trim(filter_input(INPUT_POST,'formpwd', FILTER_DEFAULT));
                $newpassConfirm = trim(filter_input(INPUT_POST,'formpwdc', FILTER_DEFAULT));

                $tokenForget = trim(filter_var($_GET['t'], FILTER_DEFAULT));
               
                if($newpass === '' || $newpass != $newpassConfirm){
                    echo '<div class="alert alert-danger">Ops, the password does not match. Check again. </br></div>';
                
                }else if (ValidateController::senhaFormato($newpass)) {

                    //gera um novo token
                    $newToken = TokenController::tokenForget();

                    //verifica hash da senha
                    $query = Conn::connect()->prepare("SELECT `user_pwd` FROM `tbuser` WHERE `forget` = ?");
                    $query->execute(array($tokenForget));
                    $info = $query->fetch();

                    if(Bcrypt::check($newpass, $info['user_pwd'])) {
                        echo '<div class="alert alert-danger">Use a different password than the current one.</div>';
                    }else{

                        $newpassCript = Bcrypt::hash($newpass);
                        $memdataup = date('Y-m-d H:i:s');
                        $log = MsgController::msg(1);

                        try{
                            $query2 = Conn::connect()->prepare("UPDATE `tbuser` SET `user_pwd` = ?, `forget` = ?, `user_dataup` = ?, `user_datauplog` = ? WHERE `forget` = ?");
                            $query2->execute(array($newpassCript, $newToken, $memdataup, $log, $tokenForget));

                            echo '<div class="alert alert-success">Password updated successfully! </br></div>';
                            echo '<p><a href="'.PATH.'"/login">New Login</a></p>';
                            die();
                            //echo "<meta HTTP-EQUIV='refresh' CONTENT='3;URL=".PATH."/login>";
                        }catch(\PDOException $erro){

                            echo 'Error. Please check with support.';
                        }
                    }
                }
            }               
        } 

        public static function editProfile (){

            if(isset($_POST['update']) == 'yes'){
                  
               $id = $_SESSION['id'];
         
               $fname = trim(filter_input(INPUT_POST, 'forfname', FILTER_UNSAFE_RAW));
               $lname = trim(filter_input(INPUT_POST, 'forlname', FILTER_UNSAFE_RAW));
               $bday = trim(filter_input(INPUT_POST, 'ubday', FILTER_UNSAFE_RAW));
               
               $dataup = date('Y-m-d H:i:s');
                  
               //Validaçoes
                       
               if(!empty($fname)){
                  $val_fname = Validatecontroller::name($fname, 'First name');  
               }else{
                  $val_fname = true;
               }
         
               if(!empty($lname)){
                  $val_lname = Validatecontroller::name($lname, 'Last name');  
               }else{
                  $val_lname = true;
               }
               
               //Log
               $log = MsgController::msg(2);
                              
                  if($val_fname == true &&
                     $val_lname == true) {
                              
                     //Query para atualização dos dados
                     $sql = Conn::connect()->prepare("UPDATE `tbuser` SET 
                                 `user_firstname` = ?,
                                 `user_lastname` = ?,
                                 `user_bday` = ?,
                                 `user_dataup` = ?,
                                 `user_datauplog` = ?
                              WHERE id = ?");
                     $sql->execute(array(
                        $fname,
                        $lname,
                        $bday,
                        $dataup,
                        $log,
                        $id));
                                       
                              //Atualiza sessão
                              echo '<div class="alert alert-success" role="alert">Profile successfully updated! </div>';
                                                            
                              echo "<meta HTTP-EQUIV='refresh' CONTENT='2;URL='>";
                              
                  }
               }
            }
    }