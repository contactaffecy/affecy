<?php

namespace App\Models;
use App\Conn;
use App\Controllers\AlertsController;
use App\Controllers\ValidateController;
use App\Controllers\EmailController;

class RegisterModel {

    public function register(){

        if(isset($_POST['register']) && $_POST['register'] == 'yes'){
            
            $_SESSION['reg-email-erro'] = NULL;
            $_SESSION['reg-email-msg'] = NULL; 
            $_SESSION['reg-pass-erro'] = NULL;
            $_SESSION['reg-pass-msg'] = NULL; 

            //$name = $_POST['uname'];
            $email = $_POST['uemail'];
            $pwd = $_POST['ucript'];

            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                //\App\Controllers\AlertsController('error','Email invalid.');
                $_SESSION['reg-email-erro'] = 1;
                AlertsController::alert('danger','Email Invalid.');
                //$_SESSION['reg-email-msg'] = 'Email Invalid.'; 
                

            }else if(strlen($pwd) < 6){
                //\App\Utility::alert('Your password is too short');
                $_SESSION['reg-pass-erro'] = 1;
                AlertsController::alert('danger','Your password is too short. (min 6 characters)');
                //$_SESSION['reg-pass-msg'] = 'Your password is too short.'; 
               
            }else if(\App\Models\UserModel::emailExist($email)){
                //\App\Utility::alert('This email is already registered.');
                $_SESSION['reg-email-erro'] = 1;
                AlertsController::alert('warning','This email is already registered.');
                //$_SESSION['reg-email-msg'] = 'This email is already registered.'; 
            
              

            }else{
                //Registrar usuário.
                $pwd = \App\Bcrypt::hash($pwd);
                $register = \App\Conn::connect()->prepare("INSERT INTO tbuser (id, user_email, user_pwd, user_dtregister) VALUES (null, ?, ?, ?)");
                $register->execute(array($email, $pwd, TODAY));

                //Notifica a administração e envia link de ativação
                $notificacaoPlus = new EmailController;
                $notificacaoPlus->emailInscricao('Novo Membro',$email, 'N/A','N/A');

                //confirma cadastro e exclui as sessoes
                unset($_SESSION['error']);
                unset($_SESSION['reg-email-erro']);
                unset($_SESSION['reg-email-msg']);
                unset($_SESSION['reg-pass-erro']);
                unset($_SESSION['reg-pass-msg']);

                $_SESSION['initial'] = 1;
                $_SESSION['reg-email'] = $email;
                  
                header('location: '.PATH.'/register/2');
                //App\Utility::redirect(PATH.'/register/2');
            }
        }
    }

        public function registerStep(){

            if(isset($_POST['reg-continue']) && $_POST['reg-continue'] == 'yes'){
                
                $_SESSION['reg-name-erro'] = NULL;
                $_SESSION['reg-name-msg'] = NULL; 
                $_SESSION['reg-nick-erro'] = NULL;
                $_SESSION['reg-nick-msg'] = NULL; 
                $_SESSION['reg-bday-erro'] = NULL;
                $_SESSION['reg-bday-msg'] = NULL; 
    
                //$name = $_POST['uname'];
                $name = trim(filter_input(INPUT_POST, 'ufname', FILTER_UNSAFE_RAW));
                $nick = trim(filter_input(INPUT_POST, 'unick', FILTER_UNSAFE_RAW));
                $bday = trim(filter_input(INPUT_POST, 'ubday', FILTER_UNSAFE_RAW));
              
                $_SESSION['ufname'] = ucfirst($name);
                $_SESSION['unick'] = strtolower($nick);
                $_SESSION['ubday'] = $bday;
                $email = $_SESSION['reg-email'];
                
                //Validação dos campos vazios

                if(empty($name)){
                    $_SESSION['reg-name-erro'] = 1;
                    AlertsController::alert('danger','Please enter your name...');
                    
                    }elseif(!Validatecontroller::name($name) || strlen($name) > 25){
                        $_SESSION['reg-name-erro'] = 1;
                        AlertsController::alert('danger','Ops, please enter your first name only :\\<br>');
                        
                    
                    }elseif(!Validatecontroller::profile($nick) || strlen($nick) > 15){
                        $_SESSION['reg-nick-erro'] = 1;
                        AlertsController::alert('danger','Use only letters and numbers in nickname...');
                        var_dump(Validatecontroller::profile($nick));
                    
                    }elseif (empty($nick)){
                        $_SESSION['reg-bday-erro'] = 1;
                        AlertsController::alert('danger','Tell us your date of birth please...');
                        
                }else{

                    //// Verifica se o cadastro já existe
                    $verProfile = Conn::connect()->prepare("SELECT `user_nick` FROM `tbuser` WHERE `user_nick` = ?");
                    $verProfile->execute(array($nick));

                    //Senao existe, vai cadastrar!
                    if($verProfile->rowCount() == 0){
                        //cadastrar
                        try{
                            $query2 = Conn::connect()->prepare("UPDATE `tbuser` SET `user_firstname` = ?, `user_nick` = ?, `user_bday` = ? WHERE `user_email` = ?");
                            $query2->execute(array($name, $nick, $bday, $email));
                               
                        }catch(\PDOException $erro){
    
                            echo 'Error';
                        }
        
                        //confirma cadastro e exclui as sessoes
                        unset($_SESSION['reg-name-erro']);
                        unset($_SESSION['reg-name-msg']); 
                        unset($_SESSION['reg-nick-erro']);
                        unset($_SESSION['reg-nick-msg']); 
                        unset($_SESSION['reg-bday-erro']);
                        unset($_SESSION['reg-bday-msg']);
                        unset($_SESSION['ufname']);
                        unset($_SESSION['unick']);
                        unset($_SESSION['ubday']);
        
                        $_SESSION['logado'][0] = 1;
                        $_SESSION['access'] = 1;
                        $_SESSION['logado'][1]; $email;

                        unset($_SESSION['confirmation']);
                        unset($_SESSION['reg-email']);
                    
                         //Encaminha usuario para home logada
                         \App\Utility::redirect(PATH.'/p/home');

                    }else{

                        echo '<div class="alert alert-warning">There is already a user with this profile name registered.</div>';
                    }
                }
            }

    }

}