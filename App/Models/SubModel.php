<?php

namespace App\Models;
use App\Conn;
use App\Controllers\AlertsController;
use App\Controllers\ValidateController;
use App\Controllers\EmailController;

class SubModel {

    public static function subscribe(){
        
        $val_name = '';
        $val_email = '';
        
        if(isset($_POST['subform'])){

                
            $name = trim(filter_input(INPUT_POST,'sbname', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            $email = trim(filter_input(INPUT_POST,'sbemail', FILTER_SANITIZE_EMAIL));
            $datareg = date('Y-m-d H:i:s');
            $country = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            $age = "99";

            $_SESSION['pt-name'] = $name;
            $_SESSION['pt-email'] = $email;
           
            //$_SESSION['pt-age'] = $age;
            
            //Validação dos campos vazios
            if(empty($name)){
                AlertsController::alert('error','Oops, tell us your name...');
                               
            }elseif (!ValidateController::text($name, 50, 'Name')) {
                $_SESSION['error'] = 1;
                $val_name = false;
                
            }elseif (empty($email) || !Validatecontroller::email($email)){
                AlertsController::alert('error','Oops, please provide a valid email address.'); 
                $val_email = false;
               
            }else{
                $val_email = true;
                $val_name = true;
                              
                if($val_name == true && $val_email == true){
                                 
                        $verificar = Conn::connect()->prepare("SELECT * FROM `tb_validation` WHERE `val_email` = ?");
                        $verificar->execute(array($email));
                         
                        if($verificar->rowCount() == 0){
                            $sql = Conn::connect()->prepare("INSERT INTO `tb_validation` (`val_id`, `val_email`,`val_name`, `val_country`, `val_age`, `val_register`) VALUES (null, ?, ?, ?, ?, ?)");
                            $sql->execute(array($email, $name, $country, $age, $datareg));
                                                            
                            //Notifica a administração e envia link de ativação
                            $notificacaoPlus = new EmailController;
                            $notificacaoPlus->emailInscricao($name, $email, $country, $age);

                            //confirma cadastro
                            unset($_SESSION['error']);
                            $_SESSION['confirmation'] = 1;
                            \App\Utility::redirect(PATH.'/confirmation');
                        
                        }else{
                            AlertsController::alert('info','Keep calm, your email is already in our database.');
                        }

                    unset($_SESSION['pt-name']);
                    unset($_SESSION['pt-email']);
                   }
            }
        } 
           
    }
}