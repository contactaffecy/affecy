<?php
    namespace App\Controllers;
    use App\Controllers\TokenController;
    use App\Views\MainView;
    use App\Conn;
    use App\Models\UserModel;
    use App\Bcrypt;
    use App\Controllers\MsgController;

    class UserController {

        public static function logado(){     
            return isset($_SESSION['logado']) ? true : false;
        }

        public function index(){

            if(self::logado() == false){
                header ('location: ../login');
                die();
            }
            
            if(isset($_SESSION['access']) == 1){
                MainView::render('user/user-home.php','','header-user.php','','My home');
            }else{
                header ('location: ../login');
                die();
            }
        }


        public static function loggout(){
            session_unset();
            session_destroy();
            header('Location:'.PATH.'/login');
            die();
        }

        public function editProfile(){
           
            if(isset($_SESSION['access']) == 1){
                MainView::render('user/user-config-profile.php','','header-user.php','','My home');
            }else{
                header ('location: ../login');
                die();
            }
        }

        public function settings(){
           
            if(isset($_SESSION['access']) == 1){
                MainView::render('user/user-config-menu.php','','header-user.php','','My home');
            }else{
                header ('location: ../login');
                die();
            }
        }

        public function editInfo(){
           
            if(isset($_SESSION['access']) == 1){
                MainView::render('user/user-config-info.php','','header-user.php','','My home');
            }else{
                header ('location: ../login');
                die();
            }
        }

        
}