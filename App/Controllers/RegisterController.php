<?php

    namespace App\Controllers;
    use App\Views\Mainview;
    class RegisterController {

       
        public function register(){
            if(isset($_SESSION['logado']) != 1) {
                MainView::render('register1.php','','','footer-simple.php','Create your account in Affecy');
            }else{
                header ('Location: '.PATH);
            }
        }

        public function registerTest(){

            MainView::render('register.php','','header-simple.php','','Create your account in Affecy');
            
        }

        public function registerStep1 (){

            if(@$_SESSION['confirmation'] == 1 || @$_SESSION['access'] == 0) {
                MainView::render('signup/reg-step1.php','','','','Lets Go');
            }else{
                MainView::render('register1.php','','','','Create your account in Affecy');
            }
            
        }


    }