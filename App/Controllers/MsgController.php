<?php
	namespace App\Controllers;
    use App\Views\Mainview;
    
class MsgController {

    public static function msg($type){
        
        switch ($type) {
            case '1':
                $msg = 'Password update.';
                break;
            case '2':
                $msg = 'Update data profile.';
                break;

            default:
                # code...
                break;
        }

        return $msg;
    }


}