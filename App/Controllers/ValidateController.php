<?php
	namespace App\Controllers;
    use App\Controllers\AlertsController;
    use \views\Mainview;
   
    
class ValidateController{

    public static function name ($post){
        
        $validar = preg_match('/^[a-zA-Z À-Úà-ú ]{2,40}$/', $post);
            if($validar)
                return true;
            else    
                return false;
    }

    public static function profile ($post){
        
        $validar = preg_match('/^[a-zA-Z0-9]{4,45}$/', $post);
            if($validar)
                return true;
            else    
                return false;
    }

    public static function telefone ($post){
        
        $post = trim($post);

        //$regexTelefone = "^[0-9]{11}$";
        $regexCel = '/[0-9]{2}[6789][0-9]{3,4}[0-9]{4}/'; // Regex para validar somente celular
        
        if (preg_match($regexCel, $post)) {
            return true;
        }else{
            AlertsController::alert('erro','Nos informe um telefone celular válido. ');
        }
    }

    public static function email ($post){
        
        if(filter_var($post, FILTER_VALIDATE_EMAIL)){
            return true;
        }else{
            return false;    
        }
    }

    public static function senha ($post){
        
        /* 1º MODELO
            (?=.*\d)(?=.*[A-Za-z0-9A-Za-z]){5,15}
            Entre o começo -> ^
            E final -> $
            da cadeia deve haver pelo menos um número -> (?=.*\d)
            e pelo menos uma letra -> (?=.*[A-Za-z])
            e tem que ser um número, uma letra ou um dos seguintes: @ @ $ $% -> [0-9A-Za-z!@#$%]
            e deve ter 8-12 caracteres -> {8,12}
        */

        /* 2º MODELO
            - primeiro lookahead ((?=.*[0-9])) garante que há pelo menos um dígito na string
            - segundo lookahead ((?=.*[a-zA-Z])) garante que há pelo menos uma letra na string
            - restante ([a-zA-Z0-9]{8,}) garante que há pelo menos 8 caracteres na string (e que todos os caracteres são letras ou números)
        */

        $validar = preg_match('/^(?=.*[0-9])(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%]{5,15}$/', $post);
            if($validar)
                return true;
            else    
                return false;
    }

    public static function senhaFormato ($senha) {
        if(empty($senha)){
            $_SESSION['senha_erro'] = 'Erro';
            AlertsController::alert('warning','Check the password. ');
           ;
        }elseif(strlen($senha) < 6){
            $_SESSION['senha_erro'] = 'Erro';
            AlertsController::alert('warning','Password too short, use 6 or more characters.');
           
        }elseif(strlen($senha) > 15){
                $_SESSION['senha_erro'] = 'Erro';   
                AlertsController::alert('warning','The password must be a maximum of 15 characters.');
               
        }elseif(!Validatecontroller::senha($senha)){
                $_SESSION['senha_erro'] = 'Erro';
                AlertsController::alert('warning','Your password must contain letters, a number and can have special characters.');
               
        }else{
            return true;
        }
    }

    public static function text ($post, $tamanho, $campo){
        
        $validar = preg_match('/^[a-zA-Z À-Úà-ú]{2,255}$/', $post);
       
            if(empty($post)){
                $_SESSION['text_error'] = 'Error';
                AlertsController::alert('error','Check the field '.$campo.' filled.');
                
            }elseif(strlen($post) <= 2){
                $_SESSION['text_error'] = 'Error';
                AlertsController::alert('error', 'Too short. Use 3 or more characters for the '.$campo.'.');
                
            }elseif(strlen($post) > $tamanho){
                    $_SESSION['text_error'] = 'Error';   
                    AlertsController::alert('erro','Too long use until '.$tamanho.' characters for the '.$campo.'.');
                
            }elseif(!$validar){
                    $_SESSION['text_error'] = 'Error';
                    AlertsController::alert('error',$campo.' cannot contain numbers and special characters, only letters.');
            }else{
                return true;
            }          
    }

    public static function textoNumero ($post, $tamanho, $campo){
        
        $validar = preg_match('/^([A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ 0-9]){3,255}$/', $post);

      
            if(empty($post)){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro','Verifique o campo '.$campo.' digitado.');
                
            }elseif(strlen($post) <= 2){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro', 'Muito curto, use 3 ou mais caracteres para o campo '.$campo.'.');
                
            }elseif(strlen($post) > $tamanho){
                    $_SESSION['text_erro'] = 'Erro';   
                    AlertsController::alert('erro','Muito longo, use até '.$tamanho.' caracteres para o '.$campo.'.');        
            
            }elseif(!$validar){
                    $_SESSION['text_erro'] = 'Erro';
                    AlertsController::alert('erro','Formato inválido no campo '.$campo.'.');
            }else{
                return true;
            }
       
    }

    public static function misto ($post, $tamanho, $campo){
        
        $validar = preg_match('/^([A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ 0-9!,-:.@#$%]){3,255}$/', $post);

      
            if(empty($post)){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro','Verifique o campo '.$campo.' digitado.');
                
            }elseif(strlen($post) <= 2){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro', 'Muito curto, use 3 ou mais caracteres para o campo '.$campo.'.');
                
            }elseif(strlen($post) > $tamanho){
                    $_SESSION['text_erro'] = 'Erro';   
                    AlertsController::alert('erro','Muito longo, use até '.$tamanho.' caracteres para o '.$campo.'.');        
            
            }elseif(!$validar){
                    $_SESSION['text_erro'] = 'Erro';
                    AlertsController::alert('erro','Formato inválido no campo '.$campo.'.');
            }else{
                return true;
            }
       
    }

    public static function livre ($post, $tamanho, $campo){
             
            if(empty($post)){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro','Verifique o campo '.$campo.' digitado.');
                
            }elseif(strlen($post) <= 2){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro', 'Muito curto, use 3 ou mais caracteres para o campo '.$campo.'.');
                
            }elseif(strlen($post) > $tamanho){
                    $_SESSION['text_erro'] = 'Erro';   
                    AlertsController::alert('erro','Muito longo, use até '.$tamanho.' caracteres para o '.$campo.'.');        
            
           
            }else{
                return true;
            }
       
    }

    public static function mistoVazio ($post, $tamanho, $campo, $vazio){
        
        $validar = preg_match('/^([A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ 0-9!,.:-@#$%]){3,255}$/', $post);

            if($vazio == true){ 
                if(empty($post)){
                    $_SESSION['text_erro'] = 'Erro';
                    AlertsController::alert('erro','Verifique o campo '.$campo.' digitado.');
                
            }elseif(strlen($post) <= 2){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro', 'Muito curto, use 3 ou mais caracteres para o campo '.$campo.'.');
                
            }elseif(strlen($post) > $tamanho){
                    $_SESSION['text_erro'] = 'Erro';   
                    AlertsController::alert('erro','Muito longo, use até '.$tamanho.' caracteres para o '.$campo.'.');        
            
            }elseif(!$validar){
                    $_SESSION['text_erro'] = 'Erro';
                    AlertsController::alert('erro','Formato ou caracteres inválidos no campo '.$campo.'.');
            }else{
                return true;
            }
        }
    }

    public static function numero ($post, $tamanho, $campo){
        
        $validar = preg_match('/[^\d\,.]/', $post);

        
            if(empty($post) || $post < 0){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro','Verifique o '.$campo.' digitado.');
            
            }elseif(strlen($post) < 1){
                $_SESSION['text_erro'] = 'Erro';
                AlertsController::alert('erro', 'Valor inválido para '.$campo.'.');
                
            }elseif(strlen($post) > $tamanho){
                    $_SESSION['text_erro'] = 'Erro';   
                    AlertsController::alert('erro','Valor inválido para '.$tamanho.' caracteres para o '.$campo.'.');
                
            }elseif(!$validar){
                    $_SESSION['text_erro'] = 'Erro';
                    AlertsController::alert('erro','Apenas números são permitidos em '.$campo.'.');   
            }else{
                return true;
            }
    }

    public static function converterMoeda($valor){
        return number_format($valor, 2,',', '.');
    }

    public static function moedaBD($valor) {
        $valor = str_replace('.','',$valor);
        $valor = str_replace(',','.',$valor);
        return $valor;
    }

    public static function celular($phone, $campo){

        $formatedPhone = preg_replace('/[^0-9]/', '', $phone);
        $matches = [];
        $validar = preg_match('/^([0-9]{2})([0-9]{4,5})([0-9]{4})$/', $formatedPhone, $matches);
        if(!$validar){
            AlertsController::alert('erro','Apenas números são permitidos em '.$campo.'.');
        }elseif ($matches) {
            return '('.$matches[1].') '.$matches[2].'-'.$matches[3];
            return $phone; // return number without format
        }
    }

    public static function formatPhone($number){
        $number="(".substr($number,0,2).") ".substr($number,2,-4)."-".substr($number,-4);
        // primeiro substr pega apenas o DDD e coloca dentro do (), segundo subtr pega os números do 3º até faltar 4, insere o hifem, e o ultimo pega apenas o 4 ultimos digitos
        return $number;
    }
}