<?php 
	namespace App\Controllers;
    use App\Views\Mainview;
    use App\Conn;

class TokenController {
    
    public static function tokenCadastro(){
        
        $geraToken = function() use(&$geraToken){
            $length = 32;
            $a = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz");
            $b = array();

            for($i = 0; $i < $length; $i++){
                $r = rand(0, (sizeof($a) - 1));
                $b[$i] = $a[$r];
            }

            $token = join("", $b);

            $query = Conn::connect()->prepare("SELECT `forget` FROM `tbuser` WHERE `forget` = ?");
            $query->execute(array($token));

                if($query->rowCount() > 0){
                    return $geraToken();
                }else{
                    return $token;
                }
        };
        
        return $token = $geraToken();       
        
    }

    public static function idPerfil(){
        
        $geraID = function() use(&$geraID){
            $length = 9;
            $a = str_split("0123456789");
            $b = array();

            for($i = 0; $i < $length; $i++){
                $r = rand(0, (sizeof($a) - 1));
                $b[$i] = $a[$r];
            }

            $tokenID = join("", $b);
            $tokenID = 'h'.$tokenID;

            $query = Conn::connect()->prepare("SELECT `user_idprofile` FROM `tbuser` WHERE `user_idprofile` = ?");
            $query->execute(array($tokenID));

                if($query->rowCount() > 0){
                    return $geraID();
                }else{
                    return $tokenID;
                }
        };
        
        return $tokenID = $geraID();       
        
    }

    public static function tokenSecure(){
        
        $geraToken = function() use(&$geraToken){
            $length = 16;
            $a = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz");
            $b = array();

            for($i = 0; $i < $length; $i++){
                $r = rand(0, (sizeof($a) - 1));
                $b[$i] = $a[$r];
            }

            $token = join("", $b);

            $query = Conn::connect()->prepare("SELECT `plus_tokenassinatura` FROM `tb_plus.membros` WHERE `plus_tokenassinatura` = ?");
            $query->execute(array($token));

                if($query->rowCount() > 0){
                    return $geraToken();
                }else{
                    return $token;
                }
               
        };
        
        return $token = $geraToken();   
        
    }

    //Cria codigo de Recibo
    public static function reciboID(){
        
        $geraID = function() use(&$geraID){
            $length = 9;
            $a = str_split("0123456789");
            $b = array();

            for($i = 0; $i < $length; $i++){
                $r = rand(0, (sizeof($a) - 1));
                $b[$i] = $a[$r];
            }

            $tokenID = join("", $b);
            
            $query = Conn::connect()->prepare("SELECT `001_recibo_id` FROM `tb_001_rc` WHERE `001_recibo_id` = ?");
            $query->execute(array($tokenID));

                if($query->rowCount() > 0){
                    return $geraID();
                }else{
                    return $tokenID;
                }
        };
        
        return $tokenID = $geraID();       
        
    }

        //Cria link do recibo
        public static function reciboLink(){
        
            $geraID = function() use(&$geraID){
                $length = 15;
                $a = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789");
                $b = array();
    
                for($i = 0; $i < $length; $i++){
                    $r = rand(0, (sizeof($a) - 1));
                    $b[$i] = $a[$r];
                }
    
                $tokenID = join("", $b);
                
                $query = Conn::connect()->prepare("SELECT `001_recibo_id` FROM `tb_001_rc` WHERE `001_recibo_id` = ?");
                $query->execute(array($tokenID));
    
                    if($query->rowCount() > 0){
                        return $geraID();
                    }else{
                        return $tokenID;
                    }
            };
            
            return $tokenID = $geraID();       
            
        }

        //Cria token do Anuncio
        public static function tokenWork(){

            $geraID = function() use(&$geraID){
                $length = 10;
                $a = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz0123456789");
                $b = array();
    
                for($i = 0; $i < $length; $i++){
                    $r = rand(0, (sizeof($a) - 1));
                    $b[$i] = $a[$r];
                }
    
                $tokenID = join("", $b);
                
                $query = Conn::connect()->prepare("SELECT `wk_idtoken` FROM `tb_membros.work` WHERE `wk_idtoken` = ?");
                $query->execute(array($tokenID));
    
                    if($query->rowCount() > 0){
                        return $geraID();
                    }else{
                        return $tokenID;
                    }
            };
            
            return $tokenID = $geraID();       
            
        }

        public static function tokenForget(){

            $codeForget = (md5(uniqid(rand(), true)));

            return $codeForget;
        }
}