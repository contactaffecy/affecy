<?php 
    namespace App\Controllers;
    use \PHPMailer\PHPMailer\PHPMailer;
    use \PHPMailer\PHPMailer\SMTP;


class EmailController extends PHPMailer{
    private $mail = null;
    
    public function ConfigEmail(){
       
        $this->mail = new PHPMailer();
        $this->mail->isSMTP();

        $this->mail->Port = EMAIL_PORTA;
        $this->mail->Host = EMAIL_HOST;
        $this->mail->isHTML(true);
        $this->mail->SMTPSecure = EMAIL_SMTP;
        $this->mail->Mailer = EMAIL_MAILER;
        $this->mail->CharSet = EMAIL_CHARSET;

        $this->mail->SMTPAuth = true;
        $this->mail->Username = EMAIL_USUARIO;
        $this->mail->Password = EMAIL_SENHA;
        $this->mail->SingleTo = true;

        $this->mail->From = EMAIL_USUARIO;
        $this->mail->FromName = EMAIL_FROM;

    }

    public function EmailCadastro ($nome, $email, $token){
        $this->ConfigEmail();

        $file = file_get_contents("../app/views/pages/includes/html/cadastroEmail.html");
        $file = str_replace("[MEMBRO]", strtoupper($nome), $file);
        $file = str_replace("[URL]", URL, $file);
        $file = str_replace("[TOKEN]", $token, $file);
        
        $this->mail->addAddress($email);
        $this->mail->Subject = "Bem vindo a Horky - Confirme seu cadastro";
        $this->mail->Body = $file;
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            return false;
            
            //die('Houve um erro no envio do email de confirmação');
        }   
            return true;
        
    }

    public function novoToken ($nome, $email, $token){
        $this->ConfigEmail();

        $file = file_get_contents("../app/views/pages/includes/html/novoToken.html");
        $file = str_replace("[MEMBRO]", strtoupper($nome), $file);
        $file = str_replace("[URL]", URL, $file);
        $file = str_replace("[TOKEN]", $token, $file);
        
        $this->mail->addAddress($email);
        $this->mail->Subject = "Bem vindo a Horky - Nova confirmação de cadastro";
        $this->mail->Body = $file;
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            return false;
            
            //die('Houve um erro no envio do email de confirmação');
        }   
            return true;
        
    }

    public function EmailAdm (){
        $this->ConfigEmail();
        
        $this->mail->addAddress('comunidade.horky@gmail.com');
        $this->mail->Subject = "Novo Cadastro Horky";
        $this->mail->Body = "Um novo cadastro foi realizado!";
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            return false;
            
            //die('Houve um erro no envio do email de confirmação');
        }   
            return true;
        
    }

    public function EmailDirect ($titulo, $mensagem){
        $this->ConfigEmail();
        
        $this->mail->addAddress('comunidade.horky@gmail.com');
        $this->mail->Subject = $titulo;
        $this->mail->Body = $mensagem;
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            return false;
            
            //die('Houve um erro no envio do email de confirmação');
        }   
            return true;
        
    }

    public function emailForget ($nome, $email, $token){
        $this->ConfigEmail();

        $file = file_get_contents('../App/views/pages/includes/html/forget.html');
        $file = str_replace("[MEMBRO]", $nome, $file);
        $file = str_replace("[URL]", URL, $file);
        $file = str_replace("[TOKEN]", $token, $file);
        
        $this->mail->addAddress($email);
        $this->mail->Subject = "Password reset - Affecy.com";
        $this->mail->Body = $file;
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            return false;
            
            //die('Houve um erro no envio do email de confirmação');
        }   
            return true;
    }

    public function EmailContato (){
        $this->ConfigEmail();

        if(isset($_POST['sendemail'])){

            $captcha = trim(filter_input(INPUT_POST,'captcha', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            if($captcha == NULL){
             
                $file = file_get_contents('../app/views/pages/includes/html/contatoEmail.html');
                $nome = trim(filter_input(INPUT_POST,'nome', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
                $email = trim(filter_input(INPUT_POST,'email', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
                $whatsapp = trim(filter_input(INPUT_POST,'whatsapp', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
                $assunto = trim(filter_input(INPUT_POST,'assunto', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
                $mensagem = trim(filter_input(INPUT_POST,'mensagem', FILTER_SANITIZE_FULL_SPECIAL_CHARS));

                //Substituição dos textos do HTML 
                $file = str_replace("[NOME]", $nome, $file);
                $file = str_replace("[EMAIL]", $email, $file);
                $file = str_replace("[WHATSAPP]", $whatsapp, $file);
                $file = str_replace("[ASSUNTO]", $assunto, $file);
                $file = str_replace("[MENSAGEM]", $mensagem, $file);
                
                $confirmacao = '<div class="alert alert-success" role="alert">Seu e-mail de contato foi enviado com sucesso.
                                Em breve retornaremos.</div>';
                
                $this->mail->addAddress(EMAIL_USUARIO);
                $this->mail->Subject = 'CONTATO RECEBIDO: '. substr(strip_tags($assunto),0,20).'...';
                $this->mail->Body = $file;
                
                if(!$this->mail->send()){
                    //echo $this->mail->ErrorInfo;
                    //return false;
                    die('Houve um erro no envio do email de confirmação. Tente novamente por favor.');
                }   
                    echo '<div class="alert alert-success" role="alert">Seu e-mail de contato foi enviado com sucesso.
                    Em breve retornaremos.</div>';
                    echo '<a href="./" title="Voltar para página inicial" alt="Voltar para página inicial">Voltar</a>';
                    die();
            }
        }
    }

    public function EmailContatoMembro (){
        $this->ConfigEmail();

        
             
            $file = file_get_contents('../app/views/pages/includes/html/contatoEmailMembro.html');
            $membro = trim(filter_input(INPUT_POST,'nomeUser', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            $nome = trim(filter_input(INPUT_POST,'nome', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            $email = trim(filter_input(INPUT_POST,'email', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            $whatsapp = trim(filter_input(INPUT_POST,'whatsapp', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            $assunto = trim(filter_input(INPUT_POST,'assunto', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            $mensagem = trim(filter_input(INPUT_POST,'mensagem', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            $destinatario = trim(filter_input(INPUT_POST,'emailUser', FILTER_SANITIZE_FULL_SPECIAL_CHARS));

            //Substituição dos textos do HTML 
            $file = str_replace("[MEMBRO]", $membro, $file);
            $file = str_replace("[NOME]", $nome, $file);
            $file = str_replace("[EMAIL]", $email, $file);
            $file = str_replace("[WHATSAPP]", $whatsapp, $file);
            $file = str_replace("[ASSUNTO]", $assunto, $file);
            $file = str_replace("[MENSAGEM]", $mensagem, $file);
            
            $confirmacao = '<div class="alert alert-success" role="alert">Seu contato foi enviado com sucesso. 
            <i class="fas fa-check-double"></i></div>';
            
            $this->mail->addAddress($destinatario);
            $this->mail->Subject = 'Horky Brasil | Contato Recebido: '. substr(strip_tags($assunto),0,10).'...';
            $this->mail->Body = $file;
            
            if(!$this->mail->send()){
                //echo $this->mail->ErrorInfo;
                //return false;
                die('Houve um erro no envio do email de confirmação. Tente novamente por favor.');
            }   
                echo '<div class="alert alert-success text-center" role="alert">Seu e-mail de contato foi enviado com sucesso.
                </div>';
                //echo "<meta HTTP-EQUIV='refresh' CONTENT='3;URL=../'>";
        
    }

    public function EmailNotification ($tipoContato, $usuario, $email, $assunto, $mensagem){
        $this->ConfigEmail();
            $file = file_get_contents('../app/views/pages/includes/html/emailNotification.html');
            //Substituição dos textos do HTML 
            $file = str_replace("[NOME]", $usuario, $file);
            $file = str_replace("[ASSUNTO]", $tipoContato.' - '.$assunto, $file);
            $file = str_replace("[EMAIL]", $email, $file);
            $file = str_replace("[MENSAGEM]", $mensagem, $file);
            
            /*$confirmacao = '<div class="alert alert-success" role="alert">Seu e-mail de contato foi enviado com sucesso.
                            Em breve retornaremos.</div>';*/
            
            $this->mail->addAddress(EMAIL_USUARIO);
            $this->mail->Subject = $tipoContato.': '. substr(strip_tags($mensagem),0,10).'...';
            $this->mail->Body = $file;
            
            if(!$this->mail->send()){
                //echo $this->mail->ErrorInfo;
                //return false;
                die('Houve um erro. Tente novamente por favor.');
            }
    }

    public function primeiroLogin ($nome, $email, $plus){
        $this->ConfigEmail();

        //$msg = 'Para aumentar sua experiência, você terá por '.$plus.' dia(s) as vantagens Premium, como: O dobro de serviços e anúncios publicados, um perfil com design diferenciado, QR Code para você imprimir e ter suas informações também no físico, selo especial de autenticidade, livre de publicidade e uma série de coisas novas que vem por aí.';
        $msg = '';
        
        $file = file_get_contents("../app/views/pages/includes/html/boasvindas.html");
        $file = str_replace("[NOME]", strtoupper($nome), $file);
       
        $file = str_replace("[MSG]", $msg, $file);
              
        
        $this->mail->addAddress($email);
        $this->mail->Subject = ucfirst($nome).", conte conosco para surpreender!";
        $this->mail->Body = $file;
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            //return false;
            
            die('Houve um erro no login');
        }   
            return true;
        
    }

    //Confirma cadastro de assinatura
    public function EmailAtivaAssinatura ($nome, $tipo, $valor, $token){
        $this->ConfigEmail();

        $file = file_get_contents("../app/views/pages/includes/html/ativacaoPlus.html");
        $file = str_replace("[MEMBRO]", strtoupper($nome), $file);
        $file = str_replace("[URL]", URL, $file);
        $file = str_replace("[TIPO]", $tipo, $file);
        $file = str_replace("[VALOR]", $valor, $file);
        $file = str_replace("[TOKEN]", $token, $file);
        
        $this->mail->addAddress(EMAIL_ADM);
        $this->mail->Subject = "Ativação de Assinatura - ".$nome;
        $this->mail->Body = $file;
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            return false;
            
            //die('Houve um erro no envio do email de confirmação');
        }   
            return true;
        
    }

    public function emailInscricao ($nome, $email, $country, $age){
        $this->ConfigEmail();
        
        $this->mail->addAddress('contactaffecy@gmail.com');
        $this->mail->Subject = "New Affecy inscription";
        $this->mail->Body = "New register! <br> Name: ".$nome." <br>Email: ".$email." <br>Country: ".$country." <br>Age: ".$age;
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            return false;
            
            //die('Houve um erro no envio do email de confirmação');
        }   
            return true;
        
    }

    public function envioRecibo ($recibo, $membro, $valor, $link, $token, $perfil, $email){
        $this->ConfigEmail();

        $file = file_get_contents('../app/views/pages/includes/html/recibodigital.html');
        $file = str_replace("[RECIBO]", $recibo, $file);
        $file = str_replace("[MEMBRO]", $membro, $file);
        $file = str_replace("[VALOR]", (ValidateController::converterMoeda($valor)), $file);
        $file = str_replace("[URL]", $link, $file);
        $file = str_replace("[TOKEN]", $token, $file);
        $file = str_replace("[PERFIL]", $perfil, $file);
               
        $this->mail->addAddress($email);
        $this->mail->Subject = "Recibo de Prestação de Serviço - Nº".$recibo;
        $this->mail->Body = $file;
        
        if(!$this->mail->send()){
            //echo $this->mail->ErrorInfo;
            return false;
            
            //die('Houve um erro no envio do email de confirmação');
        }   
            return true;
    }
}