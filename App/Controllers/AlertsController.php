<?php
	namespace App\Controllers;
    use App\Views\Mainview;
    
class AlertsController {

    public static function alert($tipo, $mensagem){
        if($tipo == 'success'){
            echo '<div class="alert alert-success" role="alert"><i class="fa fa-check"></i> '.$mensagem.'</div>';
        }else if($tipo == 'warning'){
            echo '<div class="alert alert-warning" role="alert">'.$mensagem.'</div>';
        }else if($tipo == 'danger'){
            echo '<div class="alert alert-danger" role="alert">'.$mensagem.'</div>';
        }else if($tipo == 'info'){
            echo '<div class="alert alert-info" role="alert">'.$mensagem.'</div>'; 
        }
    }


}