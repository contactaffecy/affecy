<?php
    namespace App\Controllers;
    use App\Views\MainView;

    class HomeController {

        public function homeSub(){
            //MainView::render('home.php','','','footer-simple.php','Affecy - The next level of the match');
            MainView::render(HOME,'','header.php','footer.php','Affecy - The next level of the match');
        }

        public function home(){
            MainView::render(HOME,'','header.php','footer.php','Affecy - The next level of the match');
        }

        public function learnMore(){
            MainView::render('learnmore.php','','header.php','footer.php','Affecy - Learn More');
        }

        public function error(){
            MainView::render(HOME,'','','','Affecy - The next level of the match');
        }

        public function index(){
           
            //loggout
            if(isset($_GET['loggout'])){
                session_unset();
                session_destroy();

                \App\Utility::redirect(PATH);
            }
            
            if(isset($_SESSION['user-login'])){
                //renderiza a home do usuário
                MainView::render('home1.php');
            }else{
                
                //renderiza para logar conta.
                if(isset($_POST['login'])){
                    $login = $_POST['uemail'];
                    $pass = $_POST['ucript'];

                    //verificar no banco de dados
                    $verify = \App\Conn::connect()->prepare("SELECT * FROM tbuser WHERE user_email = ?");
                    $verify->execute(array($login));

                    if($verify->rowCount() == 0){
                        //não existe o usuário!
                        \App\Utility::alert('Email or password is incorrect.');
                        \App\Utility::redirect(PATH.'/login');

                    }else{
                        $data = $verify->fetch();
                        $passBase = $data['user_pwd'];

                        if(\App\Bcrypt::check($pass, $passBase)){
                            //usuario logado com sucesso
                            $_SESSION['user-login'] = $data['user_email'];
                           \App\Utility::alert('successfully logged in teste.');
                            /*\App\Utility::redirect(PATH);*/

                            header("Location: ".PATH);
                           

                        }else{
                            //senha não confere
                            \App\Utility::alert('Email or password is incorrect (senha).');
                            \App\Utility::redirect(PATH.'/login');
                        }
                    }
                }
                
                //Senao existir $_POST, redirecionar
                \App\Utility::redirect(PATH);
            }

        }

        public function subConfirmation(){

            if(isset($_SESSION['confirmation'])){
                MainView::render('subsconfirm.php');
                unset($_SESSION['confirmation']);
            }else{
                \App\Utility::redirect(PATH);
            }
            
        }

        public function login(){

            MainView::render('login.php','','','','Login to your Affecy');
           
        }

        public function passRecovery (){
            if(isset($_SESSION['logado']) != 1) {
                MainView::render('signup/pass-reset.php','','header-mobile.php','footer-simple.php','Reset Password');
            }else{
                header ('Location: '.PATH);
            }
        }

        public function passCheck (){

            MainView::render('signup/pass-check.php','','header-mobile.php','footer-simple.php','Check your mail');
            
        }

        public function passNew (){

            MainView::render('signup/pass-new.php','','header-mobile.php','footer-simple.php','Create new password');
            
        }

        public function teste (){

            MainView::render('user/user-setting.php','','header.php','footer-simple.php','Teste');
            
        }
    }