<?php
namespace App;

    class Application
    {
        private $controller;

        private function setApp(){
            
            $loadName = 'App\Controllers\\';
            $url = explode('/', @$_GET['url']);

            if($url[0] == '') {  //se primeira parte da URL estiver vazia, volta para home
                $loadName.='Home';

            }else{ //senao tiver vazia, pega a primeira parte da URL para carregar a controller
                $loadName.=ucfirst(strtolower($url[0])); 
            }

            $loadName.='Controller'; //acrescentar controller no fim para carregar o arquivo na controller
                   
            //Senao existir o arquivo Controller.php, exibir página de erro
            if(file_exists('..\\'.$loadName.'.php')){
                
                $this->controller = new $loadName();
                $this->controller->index();

            }else{
                include ('Views/pages/includes/html/404.html');
                //die('Não existe a página solicitada!');
                
            }
           
           die();
        }

        public function run() {
            $this->setApp();
            $this->controller->index();
        }
    }
